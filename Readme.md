## Email Service - shared component across the system

**Status: In Development.**

This is very simple document related to initial phases of documentation and samples how to use and works with a new EmailService application that build as POC for now but growing every time.

Goals here:
1. Create an independent application as service that can be used to send emails from all applications (in general from .NET C# application and later maybe from FE application).
2. Build the NuGet packages to share with team to add Email functionality.
3. Has one place to configure and manage logic and configuration of Email sending. Ideally - without a lot of updates in consumer applications.

---

## Table of contents

Other documentation parts related to project and sample how to use it.

1. [Initial notes](\docs\InitialNotes.md) - some proposed solutions and started point to Email Service.
1. [EmailService - API](\docs\EmailServiceApi.md) - information about core of the service - API.
2. [EmailService - Templates](\docs\Templates.md) - information about templates.
3. [EmailService - Build and Run](\docs\BuildAndRun.md) - some notes and details on builds.
4. [EmailService - Design](\docs\ServiceDesign.md) - information about actual design for service and implemented solution. 

## Development notes

Application build with .NET Core 2.2 and .NET Standard 2.0. It means that here used latest tools and libraries and of cause platform from Microsoft (open-source and cross platform).

Requirements (frameworks need to be installed):

* [.NET Core SDK 2.2](https://dotnet.microsoft.com/download) - it will install SDK and dotnet cli.

Tools for development:

* [Visual Studio 2017 (15.9+)](https://visualstudio.microsoft.com)
* [Visual Studio Code](https://code.visualstudio.com/)
* [nuget cli](https://docs.microsoft.com/en-us/nuget/tools/nuget-exe-cli-reference)

## Links

1. [ASP.NET Core documentation](https://docs.microsoft.com/en-us/aspnet/?view=aspnetcore-2.2#pivot=core)
2. [.NET Standard](https://docs.microsoft.com/en-us/dotnet/standard/net-standard)
3. [.NET Core Guides](https://docs.microsoft.com/en-us/dotnet/core/)
