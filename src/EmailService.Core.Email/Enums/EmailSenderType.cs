﻿namespace EmailService.Core.Email.Enums
{
    /// <summary>
    /// Types of he sender supported by application. Email Provider types.
    /// </summary>
    public enum EmailSenderType
    {
        /// <summary>
        /// Generic - for all default cases using SMTP base classes.
        /// </summary>
        Generic = 0,

        /// <summary>
        /// Custom provider that use SendGrid libraries.
        /// </summary>
        SendGrid = 1
    }
}
