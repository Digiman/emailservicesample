﻿using EmailService.Core.Email.Interfaces;
using EmailService.Core.Email.Validators;
using Microsoft.Extensions.DependencyInjection;

namespace EmailService.Core.Email
{
    /// <summary>
    /// Configuration for Email module.
    /// </summary>
    public static class EmailModuleConfig
    {
        /// <summary>
        /// Configure all dependencies for Email module.
        /// </summary>
        /// <param name="serviceCollection">Services in container.</param>
        private static void Configure(IServiceCollection serviceCollection)
        {
            // register providers
            serviceCollection.AddSingleton<IEmailSenderProvider, EmailSenderProvider>();

            serviceCollection.AddSingleton<IEmailSenderConfigurationProvider, EmailSenderConfigurationProvider>();

            // register validators
            serviceCollection.AddSingleton<IModelsValidator, ModelsValidator>();
        }

        /// <summary>
        /// Configure DI for Email module components.
        /// </summary>
        /// <param name="serviceCollection">Services in container.</param>
        public static void InitializeEmailModule(this IServiceCollection serviceCollection)
        {
            Configure(serviceCollection);
        }
    }
}
