﻿using System.Threading.Tasks;
using EmailService.Core.Data.Models;
using EmailService.Core.Email.Interfaces;
using EmailService.Core.Email.Models;
using Microsoft.Extensions.Logging;

namespace EmailService.Core.Email.Senders
{
    // TODO: here it possible to improve and customize sending email with SendGrid library

    // See more details here: https://sendgrid.com/docs/for-developers/sending-email/
    // and here: https://docs.microsoft.com/en-us/azure/sendgrid-dotnet-how-to-send-email

    /// <summary>
    /// Implementation of the email sender to send email via SendGrid service.
    /// </summary>
    public sealed class SendGridEmailSender : EmailSenderBase, IEmailSender
    {
        private readonly ILogger<SendGridEmailSender> _logger;

        /// <summary>
        /// Some specific options to send emails via SendGrid email sender.
        /// </summary>
        private SendGridEmailOptions _emailOptions;

        public SendGridEmailSender(IEmailSenderConfigurationProvider emailSenderConfigurationProvider,
            ILogger<SendGridEmailSender> logger) : base(emailSenderConfigurationProvider)
        {
            _logger = logger;
            InitializeOptions();
        }

        private void InitializeOptions()
        {
            _emailOptions = EmailSenderConfigurationProvider.GetSendGridEmailOptions();
        }

        public void SendEmail(EmailModel model)
        {
            throw new System.NotImplementedException();
        }

        public Task SendEmailAsync(EmailModel model)
        {
            throw new System.NotImplementedException();
        }
    }
}
