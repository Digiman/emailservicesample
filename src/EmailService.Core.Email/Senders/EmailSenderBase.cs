﻿using EmailService.Core.Email.Interfaces;

namespace EmailService.Core.Email.Senders
{
    // TODO: implement here the base logic to wok with emails!

    /// <summary>
    /// Base class with shared logic for all email senders.
    /// </summary>
    public abstract class EmailSenderBase
    {
        /// <summary>
        /// Provider to work with email sender configurations.
        /// </summary>
        protected readonly IEmailSenderConfigurationProvider EmailSenderConfigurationProvider;

        protected EmailSenderBase(IEmailSenderConfigurationProvider emailSenderConfigurationProvider)
        {
            EmailSenderConfigurationProvider = emailSenderConfigurationProvider;
        }
    }
}
