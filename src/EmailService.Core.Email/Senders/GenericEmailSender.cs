﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using EmailService.Core.Data.Models;
using EmailService.Core.Email.Helpers;
using EmailService.Core.Email.Interfaces;
using EmailService.Core.Email.Models;
using Microsoft.Extensions.Logging;

namespace EmailService.Core.Email.Senders
{
    // TODO: implement more extendible email sender service - add more logic to create email message with parameters

    /// <summary>
    /// Generic service that use base classes from .NET BCL to send emails.
    /// </summary>
    public sealed class GenericEmailSender : EmailSenderBase, IEmailSender
    {
        private readonly ILogger<GenericEmailSender> _logger;

        /// <summary>
        /// Some specific options to send emails via Generic email sender.
        /// </summary>
        private GenericEmailOptions _emailOptions;

        public GenericEmailSender(IEmailSenderConfigurationProvider emailSenderConfigurationProvider,
            ILogger<GenericEmailSender> logger) : base(emailSenderConfigurationProvider)
        {
            _logger = logger;
            InitializeOptions();
        }

        private void InitializeOptions()
        {
            _emailOptions = EmailSenderConfigurationProvider.GetGenericEmailOptions();
        }

        public void SendEmail(EmailModel model)
        {
            _logger.LogInformation("Creating mail message...");

            // 1. Generate Mail message to send.
            MailMessage mail = CreateMailMessage(model.FromEmail, model.FromDisplayName,
                model.To, model.Subject, model.Body,
                model.Cc, model.Bcc, model.Reply,
                _emailOptions.DefaultSenderEmail, _emailOptions.DefaultSenderDisplayName, _emailOptions.UseHtml);

            // 1.1. Add attachments if available.
            if (model.Attachments.Any())
            {
                _logger.LogInformation("Adding attachments...");

                AddAttachmentsToMail(mail, model.Attachments);
            }

            _logger.LogInformation("Initializing SMTP client...");

            // 2. Get SMTP client.
            SmtpClient client = CreateSmtpClient(_emailOptions.Domain, _emailOptions.Port,
                _emailOptions.RequiresAuthentication,
                _emailOptions.UserName, _emailOptions.Password, _emailOptions.UseSsl);

            _logger.LogInformation("Sending email...");

            // 3. Send email using client.
            client.Send(mail);
        }
        
        public async Task SendEmailAsync(EmailModel model)
        {
            _logger.LogInformation("Creating mail message...");

            // 1. Generate Mail message to send.
            MailMessage mail = CreateMailMessage(model.FromEmail, model.FromDisplayName,
                model.To, model.Subject, model.Body,
                model.Cc, model.Bcc, model.Reply, 
                _emailOptions.DefaultSenderEmail, _emailOptions.DefaultSenderDisplayName, _emailOptions.UseHtml);

            // 1.1. Add attachments if available.
            if (model.Attachments.Any())
            {
                _logger.LogInformation("Adding attachments...");

                AddAttachmentsToMail(mail, model.Attachments);
            }

            _logger.LogInformation("Initializing SMTP client...");

            // 2. Get SMTP client.
            SmtpClient client = CreateSmtpClient(_emailOptions.Domain, _emailOptions.Port,
                _emailOptions.RequiresAuthentication,
                _emailOptions.UserName, _emailOptions.Password, _emailOptions.UseSsl);

            _logger.LogInformation("Sending email...");

            // 3. Send email using client.
            await client.SendMailAsync(mail);
        }

        #region Helpers.

        private static MailMessage CreateMailMessage(string fromEmail, string fromDisplayName,
            string toEmail, string subject, string message,
            string cc, string bcc, string reply,
            string defaultSenderEmail, string defaultSenderDisplayName = null, bool useHtml = true)
        {
            // identify proper sender
            var sender = IdentifySender(fromEmail, fromDisplayName, defaultSenderEmail, defaultSenderDisplayName);

            // create base email message object
            var mail = new MailMessage
            {
                From = sender,
                Subject = subject,
                Body = message,
                IsBodyHtml = useHtml
            };
            // add To
            mail.To.Add(toEmail.Replace(';', ','));

            // add CC here
            if (!string.IsNullOrEmpty(cc))
            {
                mail.CC.Add(cc.Replace(';', ','));
            }

            // add BCC here
            if (!string.IsNullOrEmpty(bcc))
            {
                mail.CC.Add(bcc.Replace(';', ','));
            }

            // add reply (list)
            if (!string.IsNullOrEmpty(reply))
            {
                mail.ReplyToList.Add(reply.Replace(';', ','));
            }
            
            return mail;
        }

        /// <summary>
        /// Identify proper sender. In case if it not provided in the model it wil use default values from configuration.
        /// </summary>
        /// <param name="fromEmail">Email from.</param>
        /// <param name="fromDisplayName">From display name.</param>
        /// <param name="defaultSenderEmail">Default sender email.</param>
        /// <param name="defaultSenderDisplayName">Default display name.</param>
        /// <returns>Returns mail address object with provided or default sender.</returns>
        private static MailAddress IdentifySender(string fromEmail, string fromDisplayName,
            string defaultSenderEmail, string defaultSenderDisplayName)
        {
            MailAddress sender;

            if (string.IsNullOrEmpty(fromEmail))
            {
                if (string.IsNullOrEmpty(defaultSenderEmail))
                {
                    throw new ArgumentException("No sender mail address was provided");
                }

                sender = !string.IsNullOrEmpty(defaultSenderDisplayName)
                    ? new MailAddress(defaultSenderEmail, defaultSenderDisplayName)
                    : new MailAddress(defaultSenderEmail);
            }
            else
            {
                sender = !string.IsNullOrEmpty(fromDisplayName)
                    ? new MailAddress(fromEmail, fromDisplayName)
                    : new MailAddress(fromEmail);
            }

            return sender;
        }

        /// <summary>
        /// Add attachments
        /// </summary>
        /// <param name="mail">Mail message to modify.</param>
        /// <param name="attachments">List of files to attach to email.</param>
        private void AddAttachmentsToMail(MailMessage mail, List<EmailAttachment> attachments)
        {
            foreach (var attachment in attachments)
            {
                mail.Attachments.Add(EmailAttachmentHelper.CreateAttachment(attachment));
            }
        }
        
        private static SmtpClient CreateSmtpClient(string host, int port, bool requiresAuthentication = true,
            string userName = null, string userKey = null, bool useSsl = false)
        {
            var client = new SmtpClient();

            if (string.IsNullOrEmpty(host))
            {
                throw new ArgumentException("No domain was provided");
            }

            client.Host = host;

            if (port > -1)
            {
                client.Port = port;
            }

            client.UseDefaultCredentials = !requiresAuthentication;

            if (requiresAuthentication)
            {
                if (string.IsNullOrEmpty(userName))
                {
                    throw new ArgumentException("No user name was provided");
                }

                client.Credentials = new NetworkCredential(userName, userKey);
            }

            client.EnableSsl = useSsl;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;

            return client;
        }

        #endregion
    }
}
