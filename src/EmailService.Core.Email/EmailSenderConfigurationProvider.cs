﻿using System.Linq;
using EmailService.Core.Common.Configuration;
using EmailService.Core.Common.Interfaces;
using EmailService.Core.Email.Interfaces;
using EmailService.Core.Email.Models;

namespace EmailService.Core.Email
{
    // TODO: implement the logic to get specific configuration for email senders (providers)

    /// <summary>
    /// Simple provider to get specific configuration for available providers to send emails.
    /// </summary>
    public sealed class EmailSenderConfigurationProvider : IEmailSenderConfigurationProvider
    {
        /// <summary>
        /// Application configuration from the config files.
        /// </summary>
        private readonly IApplicationConfigurationProvider _applicationConfigurationProvider;

        // TODO: update default provider settings for specific company resources! (not shared for all teams and projects)

        /// <summary>
        /// Default provider settings in case if not provided in configuration file!
        /// </summary>
        private readonly ProviderConfiguration _defaultProviderSettings = new ProviderConfiguration
        {
            Domain = "smtp.sendgrid.net",
            Port = 587,
            RequiresAuthentication = true,
            UserName = "apikey",
            Key = "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",
            UseSsl = false
        };

        public EmailSenderConfigurationProvider(IApplicationConfigurationProvider applicationConfigurationProvider)
        {
            _applicationConfigurationProvider = applicationConfigurationProvider;
        }

        /// <summary>
        /// Get email sender options for Generic provider.
        /// </summary>
        /// <returns>Returns email sender options.</returns>
        public GenericEmailOptions GetGenericEmailOptions()
        {
            var emailServiceConfiguration = _applicationConfigurationProvider.GetEmailServiceConfiguration();

            // NOTE: only for Generic provider we can specify different configuration from the file!
            var providerConfigName = _applicationConfigurationProvider.GetEmailServiceConfiguration().MailProvider;

            var providerConfig =
                emailServiceConfiguration.ProviderConfigurations.FirstOrDefault(x => x.ProviderName == providerConfigName) ??
                _defaultProviderSettings;

            var emailOptions = new GenericEmailOptions
            {
                Domain = providerConfig.Domain,
                Port = providerConfig.Port,
                RequiresAuthentication = providerConfig.RequiresAuthentication,
                UserName = providerConfig.UserName,
                Password = providerConfig.Key,
                UseSsl = providerConfig.UseSsl,
                DefaultSenderDisplayName = emailServiceConfiguration.DefaultSenderDisplayName,
                DefaultSenderEmail = emailServiceConfiguration.DefaultSenderEmail,
                UseHtml = emailServiceConfiguration.UseHtml
            };

            return emailOptions;
        }

        /// <summary>
        /// Get email sender options for SendGrid provider.
        /// </summary>
        /// <returns>Returns email sender options.</returns>
        public SendGridEmailOptions GetSendGridEmailOptions()
        {
            var emailServiceConfiguration = _applicationConfigurationProvider.GetEmailServiceConfiguration();

            var providerConfigName = _applicationConfigurationProvider.GetEmailServiceConfiguration().MailProvider;

            var providerConfig =
                emailServiceConfiguration.ProviderConfigurations.FirstOrDefault(x => x.ProviderName == providerConfigName) ??
                _defaultProviderSettings;

            var emailOptions = new SendGridEmailOptions
            {
                Domain = providerConfig.Domain,
                Port = providerConfig.Port,
                RequiresAuthentication = providerConfig.RequiresAuthentication,
                UserName = providerConfig.UserName,
                ApiKey = providerConfig.Key,
                UseSsl = providerConfig.UseSsl,
                DefaultSenderDisplayName = emailServiceConfiguration.DefaultSenderDisplayName,
                DefaultSenderEmail = emailServiceConfiguration.DefaultSenderEmail,
                UseHtml = emailServiceConfiguration.UseHtml
            };

            return emailOptions;
        }
    }
}
