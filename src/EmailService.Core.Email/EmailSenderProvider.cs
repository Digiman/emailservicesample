﻿using System;
using System.Collections.Generic;
using EmailService.Core.Email.Enums;
using EmailService.Core.Email.Interfaces;
using EmailService.Core.Email.Senders;
using Microsoft.Extensions.DependencyInjection;

namespace EmailService.Core.Email
{
    /// <summary>
    /// Provider to create instances of the needed senders to sent emails.
    /// </summary>
    public sealed class EmailSenderProvider : IEmailSenderProvider
    {
        private readonly IServiceProvider _serviceProvider;

        private readonly IDictionary<EmailSenderType, Type> _supportedSenders = new Dictionary<EmailSenderType, Type>()
        {
            {EmailSenderType.Generic, typeof(GenericEmailSender)},
            {EmailSenderType.SendGrid, typeof(SendGridEmailSender)}
        };

        public EmailSenderProvider(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IEmailSender GetEmailSender(EmailSenderType senderType)
        {
            var type = _supportedSenders[senderType];
            return (IEmailSender) ActivatorUtilities.GetServiceOrCreateInstance(_serviceProvider, type);
        }
    }
}
