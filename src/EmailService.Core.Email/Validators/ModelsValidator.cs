﻿using EmailService.Core.Data.Enums;
using EmailService.Core.Data.Models;
using EmailService.Core.Email.Helpers;
using EmailService.Core.Email.Interfaces;

namespace EmailService.Core.Email.Validators
{
    /// <summary>
    /// Implementation os the simple validator for requests in Email service.
    /// </summary>
    public sealed class ModelsValidator : IModelsValidator
    {
        /// <summary>
        /// Validate Email model with validator to make sure that all fields required in place and other rules.
        /// </summary>
        /// <param name="model">Model to validate.</param>
        /// <returns>Returns validation result with errors if occured.</returns>
        public EmailModelValidationResult ValidateEmailModel(EmailModel model)
        {
            var result = new EmailModelValidationResult();

            // use custom validator build with FluentValidations
            var validator = new EmailModelValidator();

            var validationResult = validator.Validate(model);

            if (validationResult.IsValid)
            {
                // no error - all's fine
                result.Status = ValidationStatus.Valid;
            }
            else
            {
                // errors occured - need to collect them and set bad validation status
                result.Errors = ErrorsHelper.ConvertErrors(validationResult.Errors);

                result.Status = ValidationStatus.NotValid;
            }

            return result;
        }
    }
}