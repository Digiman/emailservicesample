﻿using System;
using System.Globalization;
using System.Linq;
using System.Text.RegularExpressions;
using EmailService.Core.Data.Models;
using FluentValidation;
using FluentValidation.Validators;

namespace EmailService.Core.Email.Validators
{
    /// <summary>
    /// Fluent validation rules for EmailModel.
    /// </summary>
    public sealed class EmailModelValidator : AbstractValidator<EmailModel>
    {
        public EmailModelValidator()
        {
            // 1. Required fields.
            RuleFor(model => model.FromEmail).NotEmpty().WithMessage("From Email is required!");
            RuleFor(model => model.Subject).NotEmpty().WithMessage("Subject is required!");

            RuleFor(model => model.To).NonEmptyEmailAddresses(2, ',');

            // 2. Optional fields.
            When(model => !String.IsNullOrEmpty(model.Cc), () =>
                RuleFor(model => model.Cc).EmailAddresses(5, ',')
            );
            When(model => !String.IsNullOrEmpty(model.Bcc), () =>
                RuleFor(model => model.Bcc).EmailAddresses(5, ',')
            );

            When(model => !String.IsNullOrEmpty(model.Reply), () =>
                RuleFor(model => model.Reply).EmailAddresses(5, ',')
            );

            When(model => model.Attachments.Any(),
                () => RuleForEach(model => model.Attachments).SetValidator(new EmailAttachmentValidator()));
        }
    }

    /// <summary>
    /// Validator for EmailAttachment model.
    /// </summary>
    public sealed class EmailAttachmentValidator : AbstractValidator<EmailAttachment>
    {
        public EmailAttachmentValidator()
        {
            RuleFor(model => model.Filename).NotEmpty().WithMessage("Filename for attachment is required!");
            RuleFor(model => model.FileData).NotEmpty().WithMessage("File content is required in Base64 string format!");
            RuleFor(model => model.ContentType).NotEmpty().WithMessage("Content type need to be set!");
        }
    }

    /// <summary>
    /// Custom validator to check that at least one email provided emails in string.
    /// </summary>
    public sealed class EmailAtLeastOneValidator : PropertyValidator
    {
        private readonly char _separator;

        public EmailAtLeastOneValidator(char separator) : base("{PropertyName} must contain at least one email address.")
        {
            _separator = separator;
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue is string value)
            {
                if (String.IsNullOrEmpty(value)) return false;

                var emails = value.Split(_separator);

                // more than 1 - at least the one
                if (emails.Length < 1)
                {
                    return false;
                }
            }

            return true;
        }
    }

    /// <summary>
    /// Custom validator to check count of the emails in string.
    /// </summary>
    public sealed class EmailsCountValidator : PropertyValidator
    {
        private readonly int _max;
        private readonly char _separator;

        public EmailsCountValidator(int max, char separator) : base("{PropertyName} must contain not more than {MaxElements} emails.")
        {
            _max = max;
            _separator = separator;
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue is string value)
            {
                if (String.IsNullOrEmpty(value)) return false;

                var emails = value.Split(_separator);

                if (emails.Length > _max)
                {
                    context.MessageFormatter.AppendArgument("MaxElements", _max);
                    return false;
                }
            }

            return true;
        }
    }

    /// <summary>
    /// Validate each email in the list with separator.
    /// </summary>
    public sealed class EmailsListValidator : PropertyValidator
    {
        private readonly char _separator;

        public EmailsListValidator(char separator) : base("{PropertyName} must contains valid emails!")
        {
            _separator = separator;
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue is string value)
            {
                if (String.IsNullOrEmpty(value)) return false;

                var emails = value.Trim().Split(_separator);
                bool isValid = true;

                foreach (var email in emails)
                {
                    isValid = RegexUtilities.IsValidEmail(email.Trim());
                    if (!isValid)
                        break;
                }

                return isValid;
            }

            return false;
        }
    }

    /// <summary>
    /// Custom validator to validate email address to specific domain.
    /// Will works only for on email address in string.
    /// </summary>
    public class EmailFromDomainValidator : PropertyValidator
    {
        private readonly string _domain;

        public EmailFromDomainValidator(string domain)
            : base("Email address {PropertyValue} is not from domain {domain}")
        {
            _domain = domain;
        }

        protected override bool IsValid(PropertyValidatorContext context)
        {
            if (context.PropertyValue == null) return false;
            var split = (context.PropertyValue as string).Split('@');
            if (split.Length == 2 && split[1].ToLower().Equals(_domain)) return true;
            return false;
        }
    }

    /// <summary>
    /// Extensions for Fluent Validation with custom combination of rules.
    /// </summary>
    public static class CustomValidatorExtensions
    {
        public static IRuleBuilderOptions<T, string> EmailAddressFromDomain<T>(
            this IRuleBuilder<T, string> ruleBuilder, string domain)
        {
            return ruleBuilder.SetValidator(new EmailFromDomainValidator(domain));
        }

        public static IRuleBuilder<T, string> EmailAddresses<T>(this IRuleBuilder<T, string> ruleBuilder,
            int maxItems, char separator)
        {
            return ruleBuilder.SetValidator(new EmailsCountValidator(maxItems, separator))
                .SetValidator(new EmailsListValidator(separator));
        }

        public static IRuleBuilder<T, string> NonEmptyEmailAddresses<T>(this IRuleBuilder<T, string> ruleBuilder,
            int maxItems, char separator)
        {
            return ruleBuilder.NotEmpty()
                .SetValidator(new EmailAtLeastOneValidator(separator))
                .SetValidator(new EmailsCountValidator(maxItems, separator))
                .SetValidator(new EmailsListValidator(separator));
        }

        public static IRuleBuilder<T, string> AtLeastOneEmailAddresses<T>(this IRuleBuilder<T, string> ruleBuilder,
            char separator)
        {
            return ruleBuilder.NotEmpty()
                .SetValidator(new EmailAtLeastOneValidator(separator));
        }
    }

    /// <summary>
    /// Helpers for validate email address with Regex.
    /// </summary>
    /// <see cref="https://docs.microsoft.com/en-us/dotnet/standard/base-types/how-to-verify-that-strings-are-in-valid-email-format"/>
    public static class RegexUtilities
    {
        public static bool IsValidEmail(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
                return false;

            try
            {
                // Normalize the domain
                email = Regex.Replace(email, @"(@)(.+)$", DomainMapper,
                    RegexOptions.None, TimeSpan.FromMilliseconds(200));

                // Examines the domain part of the email and normalizes it.
                string DomainMapper(Match match)
                {
                    // Use IdnMapping class to convert Unicode domain names.
                    var idn = new IdnMapping();

                    // Pull out and process domain name (throws ArgumentException on invalid)
                    var domainName = idn.GetAscii(match.Groups[2].Value);

                    return match.Groups[1].Value + domainName;
                }
            }
            catch (RegexMatchTimeoutException e)
            {
                return false;
            }
            catch (ArgumentException e)
            {
                return false;
            }

            try
            {
                return Regex.IsMatch(email,
                    @"^(?("")("".+?(?<!\\)""@)|(([0-9a-z]((\.(?!\.))|[-!#\$%&'\*\+/=\?\^`\{\}\|~\w])*)(?<=[0-9a-z])@))" +
                    @"(?(\[)(\[(\d{1,3}\.){3}\d{1,3}\])|(([0-9a-z][-0-9a-z]*[0-9a-z]*\.)+[a-z0-9][\-a-z0-9]{0,22}[a-z0-9]))$",
                    RegexOptions.IgnoreCase, TimeSpan.FromMilliseconds(250));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
    }
}
