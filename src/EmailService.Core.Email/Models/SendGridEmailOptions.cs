﻿namespace EmailService.Core.Email.Models
{
    // TODO: add here more options for customizing SendGrid provider

    /// <summary>
    /// Specific option to use SendGrid email provider.
    /// </summary>
    public sealed class SendGridEmailOptions : EmailOptions
    {
        /// <summary>
        /// Username for using SendGrid.
        /// By default if it using with API it will be 'apikey' value.
        /// </summary>
        public string UserName { get; set; } = "apikey";

        /// <summary>
        /// Personal API key generated in the SendGrid portal.
        /// </summary>
        public string ApiKey { get; set; }
    }
}
