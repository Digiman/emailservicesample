﻿namespace EmailService.Core.Email.Models
{
    /// <summary>
    /// Base email options.
    /// </summary>
    public class EmailOptions
    {
        /// <summary>
        /// Domain - SMTP server DNS name.
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// Port for SMTP server.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Use SSL to send via secure connection?
        /// </summary>
        public bool UseSsl { get; set; }

        /// <summary>
        /// Does it needed to do authentication?
        /// </summary>
        public bool RequiresAuthentication { get; set; } = true;

        /// <summary>
        /// Default sender email.
        /// </summary>
        public string DefaultSenderEmail { get; set; }

        /// <summary>
        /// Default sender name.
        /// </summary>
        public string DefaultSenderDisplayName { get; set; }

        /// <summary>
        /// Use HTML in body of the email.
        /// </summary>
        public bool UseHtml { get; set; } = true;
    }
}
