﻿namespace EmailService.Core.Email.Models
{
    /// <summary>
    /// Generic options for service.
    /// </summary>
    public sealed class GenericEmailOptions : EmailOptions
    {
        /// <summary>
        /// User name for service. (api key name)
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Password or key to use service.
        /// </summary>
        public string Password { get; set; }
    }
}
