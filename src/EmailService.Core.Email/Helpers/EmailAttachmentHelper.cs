﻿using System;
using System.IO;
using System.Net.Mail;
using EmailService.Core.Data.Models;

namespace EmailService.Core.Email.Helpers
{
    /// <summary>
    /// Helpers class to work with attachments. Provide same logic to convert file data as in consumer library.
    /// </summary>
    public static class EmailAttachmentHelper
    {
        /// <summary>
        /// Create attachment object to include to email.
        /// </summary>
        /// <param name="attachment">Attachment model.</param>
        /// <returns>Returns objects with attachment data.</returns>
        public static Attachment CreateAttachment(EmailAttachment attachment)
        {
            return new Attachment(GetSteamFromBase64(attachment.FileData), attachment.Filename,
                attachment.ContentType);
        }

        /// <summary>
        /// Convert Base64 string to stream.
        /// </summary>
        /// <param name="fileData">String in Base64 format.</param>
        /// <returns>Returns object with a stream.</returns>
        private static Stream GetSteamFromBase64(string fileData)
        {
            var bytes = Convert.FromBase64String(fileData);
            return new MemoryStream(bytes);
        }
    }
}