﻿using System.Collections.Generic;
using EmailService.Core.Data.Models;
using FluentValidation.Results;

namespace EmailService.Core.Email.Helpers
{
    /// <summary>
    /// Simple helper to work with errors.
    /// </summary>
    public static class ErrorsHelper
    {
        public static List<ErrorDetail> ConvertErrors(IEnumerable<ValidationFailure> validationResultErrors)
        {
            var result = new List<ErrorDetail>();

            foreach (var validationResultError in validationResultErrors)
            {
                result.Add(ConvertValidationError(validationResultError));
            }

            return result;
        }

        private static ErrorDetail ConvertValidationError(ValidationFailure validationResultError)
        {
            return new ErrorDetail
            {
                Type = "ModelValidationError",
                Message = validationResultError.ErrorMessage
            };
        }
    }
}
