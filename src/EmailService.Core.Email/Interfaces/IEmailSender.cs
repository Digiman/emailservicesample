﻿using System.Threading.Tasks;
using EmailService.Core.Data.Models;

namespace EmailService.Core.Email.Interfaces
{
    /// <summary>
    /// Entry point to send emails.
    /// </summary>
    public interface IEmailSender
    {
        /// <summary>
        /// Send email based on email model.
        /// </summary>
        /// <param name="model">Information to create email to send.</param>
        void SendEmail(EmailModel model);

        /// <summary>
        /// Send email based on email model.
        /// </summary>
        /// <param name="model">Information to create email to send.</param>
        Task SendEmailAsync(EmailModel model);
    }
}
