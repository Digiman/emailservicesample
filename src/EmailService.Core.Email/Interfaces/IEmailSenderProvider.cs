﻿using EmailService.Core.Email.Enums;

namespace EmailService.Core.Email.Interfaces
{
    public interface IEmailSenderProvider
    {
        /// <summary>
        /// Get actual instance of the provider to send emails.
        /// </summary>
        /// <param name="senderType">sender/provider type.</param>
        /// <returns>Returns instance of the email sender.</returns>
        IEmailSender GetEmailSender(EmailSenderType senderType);
    }
}
