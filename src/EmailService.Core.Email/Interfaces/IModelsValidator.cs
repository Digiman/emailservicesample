﻿using EmailService.Core.Data.Models;

namespace EmailService.Core.Email.Interfaces
{
    public interface IModelsValidator
    {
        EmailModelValidationResult ValidateEmailModel(EmailModel model);
    }
}
