﻿using EmailService.Core.Email.Models;

namespace EmailService.Core.Email.Interfaces
{
    /// <summary>
    /// Provider to work with configuration for email senders/providers.
    /// </summary>
    public interface IEmailSenderConfigurationProvider
    {
        /// <summary>
        /// Get email sender options for Generic provider.
        /// </summary>
        /// <returns>Returns email sender options.</returns>
        GenericEmailOptions GetGenericEmailOptions();

        /// <summary>
        /// Get email sender options for SendGrid provider.
        /// </summary>
        /// <returns>Returns email sender options.</returns>
        SendGridEmailOptions GetSendGridEmailOptions();
    }
}
