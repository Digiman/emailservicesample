﻿using EmailService.Consumer.Helpers;
using EmailService.Consumer.Interfaces;

namespace EmailService.Consumer.Serializers
{
    /// <summary>
    /// Simple implementation of the serializer for request data (original).
    /// It can be updated in future to use different kind of serializers for requests.
    /// </summary>
    /// <remarks>
    /// Initially it uses simple JSON format.
    /// </remarks>
    public sealed class RequestSerializer : IRequestSerializer
    {
        public string SerializeRequest<T>(T request)
        {
            return SerializerHelper.SerializeToJson(request);
        }

        public T DeserializeRequest<T>(string data)
        {
            return SerializerHelper.DeserializeFromJson<T>(data);
        }
    }
}
