﻿namespace EmailService.Consumer
{
    /// <summary>
    /// Constant values across the library.
    /// </summary>
    public static class Constants
    {
        /// <summary>
        /// Default configuration file name.
        /// </summary>
        public const string DefaultConfigurationFileName = "EmailServiceConfig.json";

        /// <summary>
        /// Default API version - will be used if not provided in configuration.
        /// </summary>
        public const string DefaultApiVersion = "1.0";
    }
}
