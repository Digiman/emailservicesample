﻿using EmailService.Consumer.Interfaces;

namespace EmailService.Consumer.Consumers
{
    // TODO: implement later as additional logic to get supported templates!

    /// <summary>
    /// Simple logic to read information about templates available in EmailService.
    /// </summary>
    public sealed class TemplatesServiceConsumer : ITemplatesServiceConsumer
    {
    }
}
