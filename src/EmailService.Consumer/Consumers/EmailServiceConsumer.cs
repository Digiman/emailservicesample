﻿using System;
using System.Threading.Tasks;
using EmailService.Consumer.Configuration;
using EmailService.Consumer.DTOs;
using EmailService.Consumer.Exceptions;
using EmailService.Consumer.Executors;
using EmailService.Consumer.Interfaces;
using EmailService.Consumer.Models;
using EmailService.Consumer.Providers;

namespace EmailService.Consumer.Consumers
{
    // TODO: implement here also logic to handle exceptions!
    // TODO: rename later class (do not use Consumer here in future)

    /// <summary>
    /// Logic to use EmailService to work with sending emails.
    /// </summary>
    /// <remarks>
    /// Here is an entry point to use EmailService and send emails! We need to specify what to send and addresses and it's all.
    /// </remarks>
    public sealed class EmailServiceConsumer : IEmailServiceConsumer
    {
        /// <summary>
        /// Provider to work with endpoints for service.
        /// </summary>
        private readonly IEmailServiceEndpointProvider _emailServiceEndpointProvider;

        /// <summary>
        /// Logic to execute HTTP requests.
        /// </summary>
        private readonly IRequestExecutor _requestExecutor;

        public EmailServiceConsumer()
        {
            _requestExecutor = new RequestExecutor();
            _emailServiceEndpointProvider = new EmailServiceEndpointProvider();
        }

        public EmailServiceConsumer(EmailServiceConfiguration emailServiceConfiguration)
        {
            _requestExecutor = new RequestExecutor();
            _emailServiceEndpointProvider = new EmailServiceEndpointProvider(emailServiceConfiguration);
        }

        #region Send email.

        /// <summary>
        /// Send email with some settings without using templates.
        /// </summary>
        /// <param name="emailModel">Email model with information how to send email.</param>
        public void SendSimpleEmail(EmailModel emailModel)
        {
            try
            {
                // 1. Create Email model for service.
                var requestEmailModel = ModelsMapper.Convert(emailModel);

                // 2. Get URL to call API to send email.
                var url = _emailServiceEndpointProvider.GetSimpleEmailPath();

                // 3. Execute request.
                var result = _requestExecutor.ExecutePostRequest<EmailResultDTO, EmailModelDTO>(url, requestEmailModel);

                // 4. Analyze result.
                // TODO: do something with the result!
            }
            catch (Exception ex)
            {
                throw new EmailServiceException(ex.Message, ex);
            }
        }

        /// <summary>
        /// Send email with some settings without using templates.
        /// </summary>
        /// <param name="emailModel">Email model with information how to send email.</param>
        public async Task SendSimpleEmailAsync(EmailModel emailModel)
        {
            try
            {
                // 1. Create Email model for service.
                var requestEmailModel = ModelsMapper.Convert(emailModel);

                // 2. Get URL to call API to send email.
                var url = _emailServiceEndpointProvider.GetSimpleEmailPath();

                // 3. Execute request.
                var result = await _requestExecutor.ExecutePostRequestAsync<EmailResultDTO, EmailModelDTO>(url, requestEmailModel);

                // 4. Analyze result.
                // TODO: do something with the result!
            }
            catch (Exception ex)
            {
                throw new EmailServiceException(ex.Message, ex);
            }
        }

        #endregion

        #region Send email with template support.

        /// <summary>
        /// Send email with some settings with using template generation on the service side.
        /// </summary>
        /// <param name="emailTemplateModel">Email model with information how to send email and what template need to use.</param>
        public void SendEmailWithTemplate(EmailTemplateModel emailTemplateModel)
        {
            try
            {
                // 1. Create Email model for service.
                var requestEmailModel = ModelsMapper.Convert(emailTemplateModel);

                // 2. Get URL to call API to send email.
                var url = _emailServiceEndpointProvider.GetEmailWithTemplatePath();

                // 3. Execute request.
                var result = _requestExecutor.ExecutePostRequest<EmailResultDTO, EmailModelTemplateDTO>(url, requestEmailModel);

                // 4. Analyze result.
                // TODO: do something with the result!
            }
            catch (Exception ex)
            {
                throw new EmailServiceException(ex.Message, ex);
            }
        }

        /// <summary>
        /// Send email with some settings with using template generation on the service side.
        /// </summary>
        /// <param name="emailTemplateModel">Email model with information how to send email and what template need to use.</param>
        public async Task SendEmailWithTemplateAsync(EmailTemplateModel emailTemplateModel)
        {
            try
            {
                // 1. Create Email model for service.
                var requestEmailModel = ModelsMapper.Convert(emailTemplateModel);

                // 2. Get URL to call API to send email.
                var url = _emailServiceEndpointProvider.GetEmailWithTemplatePath();

                // 3. Execute request.
                var result = await _requestExecutor.ExecutePostRequestAsync<EmailResultDTO, EmailModelTemplateDTO>(url, requestEmailModel);

                // 4. Analyze result.
                // TODO: do something with the result!
            }
            catch (Exception ex)
            {
                throw new EmailServiceException(ex.Message, ex);
            }
        }

        #endregion
    }
}
