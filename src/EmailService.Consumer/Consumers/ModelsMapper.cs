﻿using System.Linq;
using EmailService.Consumer.DTOs;
using EmailService.Consumer.Helpers;
using EmailService.Consumer.Models;

namespace EmailService.Consumer.Consumers
{
    /// <summary>
    /// Very simple object mapper (without using AutoMapper here for now).
    /// </summary>
    public static class ModelsMapper
    {
        /// <summary>
        /// Convert consumer model to DTO that uses in Email Service API.
        /// </summary>
        /// <param name="emailModel">Consumer email model.</param>
        /// <returns>Returns DTO object with email model.</returns>
        public static EmailModelDTO Convert(EmailModel emailModel)
        {
            var dto = new EmailModelDTO
            {
                FromEmail = emailModel.FromEmail,
                FromDisplayName = emailModel.FromDisplayName,
                To = emailModel.To,
                Cc = emailModel.Cc,
                Bcc = emailModel.Bcc,
                Reply = emailModel.Reply,
                Subject = emailModel.Subject,
                Body = emailModel.Body
            };

            if (emailModel.Files.Any())
            {
                foreach (var fileModel in emailModel.Files)
                {
                    dto.Attachments.Add(EmailAttachmentHelper.CreateEmailAttachment(fileModel));
                }
            }

            return dto;
        }

        /// <summary>
        /// Convert consumer model to DTO that uses in Email Service API with using templates.
        /// </summary>
        /// <param name="emailModel">Consumer email model.</param>
        /// <returns>Returns DTO object with email model.</returns>
        public static EmailModelTemplateDTO Convert(EmailTemplateModel emailModel)
        {
            var dto = new EmailModelTemplateDTO
            {
                Model = Convert(emailModel.Model),
                TemplateName = emailModel.TemplateName,
                Data = SerializerHelper.SerializeToJson(emailModel.Data)
            };

            return dto;
        }
    }
}