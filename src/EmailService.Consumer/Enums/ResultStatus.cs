﻿namespace EmailService.Consumer.Enums
{
    /// <summary>
    /// Statuses aof processing something inside application.
    /// </summary>
    public enum ResultStatus
    {
        None = 0,
        Success = 1,
        Error = 2
    }
}
