﻿using System.Net;
using System.Threading.Tasks;
using EmailService.Consumer.Exceptions;
using EmailService.Consumer.Interfaces;
using EmailService.Consumer.Serializers;
using RestSharp;

namespace EmailService.Consumer.Executors
{
    /// <summary>
    /// Logic to execute actual HTTP requests.
    /// </summary>
    /// <remarks>
    /// Now it uses RestSharp to simplify call to API.
    /// </remarks>
    public sealed class RequestExecutor : IRequestExecutor
    {
        /// <summary>
        /// Simple serializer for requests in JSON format.
        /// </summary>
        private readonly IRequestSerializer _requestSerializer;

        public RequestExecutor()
        {
            _requestSerializer = new RequestSerializer();
        }

        #region Sync methods.

        public TResult ExecutePostRequest<TResult, TData>(string url, TData request)
        {
            var client = new RestClient(url);

            var clientRequest = new RestRequest(Method.POST);
            clientRequest.AddParameter("Content-Type", "application/json");
            clientRequest.AddParameter("Accept", "application/json");

            // add body with JSON
            clientRequest.AddJsonBody(request);

            var response = client.Execute(clientRequest);
            if (response.IsSuccessful)
            {
                // deserialize request
                return _requestSerializer.DeserializeRequest<TResult>(response.Content);
            }

            // TODO: handle error during requests more friendly
            HandleResponseError(response);

            return default(TResult);
        }

        public TResult ExecuteGetRequest<TResult>(string url)
        {
            var client = new RestClient(url);

            var clientRequest = new RestRequest(Method.GET);
            clientRequest.AddParameter("Content-Type", "application/json");
            clientRequest.AddParameter("Accept", "application/json");

            var response = client.Execute(clientRequest);
            if (response.IsSuccessful)
            {
                // deserialize request
                return _requestSerializer.DeserializeRequest<TResult>(response.Content);
            }

            // TODO: handle error during requests more friendly
            HandleResponseError(response);

            return default(TResult);
        }

        #endregion

        #region Async methods.

        public async Task<TResult> ExecutePostRequestAsync<TResult, TData>(string url, TData request)
        {
            var client = new RestClient(url);

            var clientRequest = new RestRequest(Method.POST);
            clientRequest.AddParameter("Content-Type", "application/json");
            clientRequest.AddParameter("Accept", "application/json");

            // add body with JSON
            clientRequest.AddJsonBody(request);

            var response = await client.ExecuteTaskAsync(clientRequest);
            if (response.IsSuccessful)
            {
                // deserialize request
                return _requestSerializer.DeserializeRequest<TResult>(response.Content);
            }

            // TODO: handle error during requests more friendly
            HandleResponseError(response);

            return default(TResult);
        }

        public async Task<TResult> ExecuteGetRequestAsync<TResult>(string url)
        {
            var client = new RestClient(url);

            var clientRequest = new RestRequest(Method.GET);
            clientRequest.AddParameter("Content-Type", "application/json");
            clientRequest.AddParameter("Accept", "application/json");

            var response = await client.ExecuteTaskAsync(clientRequest);
            if (response.IsSuccessful)
            {
                // deserialize request
                return _requestSerializer.DeserializeRequest<TResult>(response.Content);
            }

            // TODO: handle error during requests more friendly
            HandleResponseError(response);

            return default(TResult);
        }

        #endregion

        #region Helpers.

        private void HandleResponseError(IRestResponse response)
        {
            switch (response.StatusCode)
            {
                case HttpStatusCode.BadRequest: // 400
                    throw new HttpApiException($"Bad request! Content: {response.Content}");
                    //_logger.LogWarning($"Bad request! Content: {response.Content}");
                    //break;
                case HttpStatusCode.Unauthorized: // 401
                    throw new HttpApiException($"Not authorized! Content: {response.Content}");
                    //_logger.LogWarning($"Not authorized! Content: {response.Content}");
                    //break;
                case HttpStatusCode.Forbidden: // 403
                    throw new HttpApiException($"Forbidden! Content: {response.Content}");
                    //_logger.LogWarning($"Forbidden! Content: {response.Content}");
                    //break;
                case HttpStatusCode.NotFound: // 404
                    throw new HttpApiException($"Not Found! Content: {response.Content}");
                    //_logger.LogWarning($"Not Found! Content: {response.Content}");
                    //break;
                case HttpStatusCode.InternalServerError: // 500
                    throw new HttpApiException($"Internal server error! Content: {response.Content}");
                    //_logger.LogWarning($"Internal server error! Content: {response.Content}");
                    //break;
                default: // default - usually if response was unsuccessful
                    throw new HttpApiException("Service is unavailable!");
            }
        }

        #endregion
    }
}
