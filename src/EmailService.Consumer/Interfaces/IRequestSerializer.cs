﻿namespace EmailService.Consumer.Interfaces
{
    public interface IRequestSerializer
    {
        /// <summary>
        /// Serialize request object to JSON.
        /// </summary>
        /// <typeparam name="T">Type of the object to serialize.</typeparam>
        /// <param name="request">Object with data to serialize.</param>
        /// <returns>Returns string in JSON format.</returns>
        string SerializeRequest<T>(T request);

        /// <summary>
        /// Deserialize request object from JSON.
        /// </summary>
        /// <typeparam name="T">Type of the object to serialize.</typeparam>
        /// <param name="data">String with JSON object to deserialize.</param>
        /// <returns>Returns deserialized object.</returns>
        T DeserializeRequest<T>(string data);
    }
}
