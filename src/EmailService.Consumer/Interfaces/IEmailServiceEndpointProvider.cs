﻿namespace EmailService.Consumer.Interfaces
{
    /// <summary>
    /// Provider to use with API endpoints for EmailService.
    /// </summary>
    public interface IEmailServiceEndpointProvider
    {
        /// <summary>
        /// Get base API service path.
        /// </summary>
        /// <returns>Returns path/URI to service.</returns>
        string GetBaseServiceUrl();

        /// <summary>
        /// Get URL to send simple email.
        /// </summary>
        /// <returns>Returns path/URI to service.</returns>
        string GetSimpleEmailPath();

        /// <summary>
        /// Get URL to send email with template.
        /// </summary>
        /// <returns>Returns path/URI to service.</returns>
        string GetEmailWithTemplatePath();

        /// <summary>
        /// Get URL to get list of available templates.
        /// </summary>
        /// <returns>Returns path/URI to service.</returns>
        string GetAllTemplatesPath();

        /// <summary>
        /// Get URL to get template data.
        /// </summary>
        /// <returns>Returns path/URI to service.</returns>
        string GetTemplatePath();
    }
}
