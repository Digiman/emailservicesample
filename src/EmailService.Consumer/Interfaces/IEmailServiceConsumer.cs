﻿using System.Threading.Tasks;
using EmailService.Consumer.Models;

namespace EmailService.Consumer.Interfaces
{
    public interface IEmailServiceConsumer
    {
        /// <summary>
        /// Send email with some settings without using templates.
        /// </summary>
        /// <param name="emailModel">Email model with information how to send email.</param>
        void SendSimpleEmail(EmailModel emailModel);

        /// <summary>
        /// Send email with some settings without using templates.
        /// </summary>
        /// <param name="emailModel">Email model with information how to send email.</param>
        Task SendSimpleEmailAsync(EmailModel emailModel);

        /// <summary>
        /// Send email with some settings with using template generation on the service side.
        /// </summary>
        /// <param name="emailTemplateModel">Email model with information how to send email and what template need to use.</param>
        void SendEmailWithTemplate(EmailTemplateModel emailTemplateModel);

        /// <summary>
        /// Send email with some settings with using template generation on the service side.
        /// </summary>
        /// <param name="emailTemplateModel">Email model with information how to send email and what template need to use.</param>
        Task SendEmailWithTemplateAsync(EmailTemplateModel emailTemplateModel);
    }
}
