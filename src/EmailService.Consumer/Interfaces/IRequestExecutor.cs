﻿using System.Threading.Tasks;

namespace EmailService.Consumer.Interfaces
{
    public interface IRequestExecutor
    {
        TResult ExecutePostRequest<TResult, TData>(string url, TData request);
        TResult ExecuteGetRequest<TResult>(string url);

        Task<TResult> ExecutePostRequestAsync<TResult, TData>(string url, TData request);
        Task<TResult> ExecuteGetRequestAsync<TResult>(string url);
    }
}
