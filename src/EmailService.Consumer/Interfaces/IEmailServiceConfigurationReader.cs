﻿using EmailService.Consumer.Configuration;

namespace EmailService.Consumer.Interfaces
{
    public interface IEmailServiceConfigurationReader
    {
        EmailServiceConfiguration Load(string filename);
    }
}
