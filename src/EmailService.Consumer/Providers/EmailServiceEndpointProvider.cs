﻿using EmailService.Consumer.Configuration;
using EmailService.Consumer.Interfaces;

namespace EmailService.Consumer.Providers
{
    // TODO: implement the provider to work with URL to API

    // TODO: find the way how to deal better here with configuration that ideally need to read from file!

    /// <summary>
    /// Simple provider to generate routes that needed to work with EmailService API,
    /// </summary>
    public sealed class EmailServiceEndpointProvider : IEmailServiceEndpointProvider
    {
        /// <summary>
        /// Configuration with settings to access to service.
        /// </summary>
        private readonly EmailServiceConfiguration _emailServiceConfiguration;

        public EmailServiceEndpointProvider()
        {
            var emailServiceConfigurationReader = new EmailServiceConfigurationReader();

            _emailServiceConfiguration = emailServiceConfigurationReader.Load(null);
        }

        public EmailServiceEndpointProvider(EmailServiceConfiguration emailServiceConfiguration)
        {
            _emailServiceConfiguration = emailServiceConfiguration;
        }

        public string GetBaseServiceUrl()
        {
            return BaseApiPath;
        }

        public string GetSimpleEmailPath()
        {
            return $"{EmailPath}/SendEmailSimple";
        }

        public string GetEmailWithTemplatePath()
        {
            return $"{EmailPath}/SendEmail";
        }

        public string GetAllTemplatesPath()
        {
            return $"{TemplatesPath}";
        }

        public string GetTemplatePath()
        {
            return $"{TemplatesPath}";
        }

        #region Helpers.

        /// <summary>
        /// Path to Email controller.
        /// </summary>
        private string EmailPath
        {
            get { return $"{BaseApiPath}/api/v{ApiVersion}/email"; }
        }

        /// <summary>
        /// Path to Template controller.
        /// </summary>
        private string TemplatesPath
        {
            get { return $"{BaseApiPath}/api/v{ApiVersion}/template"; }
        }

        /// <summary>
        /// Base API path from configuration.
        /// </summary>
        private string BaseApiPath
        {
            get { return _emailServiceConfiguration.BaseApiPath; }
        }

        /// <summary>
        /// Get API Version for selected API to use.
        /// </summary>
        private string ApiVersion
        {
            get
            {
                return string.IsNullOrEmpty(_emailServiceConfiguration.ApiVersion)
                    ? Constants.DefaultApiVersion
                    : _emailServiceConfiguration.ApiVersion;
            }
        }

        #endregion
    }
}
