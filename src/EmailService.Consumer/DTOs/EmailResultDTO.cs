﻿using System.Collections.Generic;
using EmailService.Consumer.Enums;

namespace EmailService.Consumer.DTOs
{
    /// <summary>
    /// DTO Model with information about sending email.
    /// </summary>
    public sealed class EmailResultDTO
    {
        /// <summary>
        /// Status of the processing.
        /// </summary>
        public ResultStatus Status { get; set; }

        /// <summary>
        /// Error message.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// List of errors happened during processing.
        /// </summary>
        public List<ErrorDetailDTO> ErrorDetails { get; set; }
    }

    /// <summary>
    /// Error details.
    /// </summary>
    public sealed class ErrorDetailDTO
    {
        /// <summary>
        /// Error type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Error message with details about error.
        /// </summary>
        public string Message { get; set; }
    }
}
