﻿namespace EmailService.Consumer.DTOs
{
    // TODO: need to think here how to share data for template with this DTO used in API action
    
    // Maybe here we can use Data as string that in JSON format or other format to make sure that data in correct format will bes
    // In this case we can implement additional serialization provider to convert this data.

    /// <summary>
    /// Object that provide details about sending email with template.
    /// </summary>
    public sealed class EmailModelTemplateDTO
    {
        /// <summary>
        /// Model with details how to send emails.
        /// </summary>
        public EmailModelDTO Model { get; set; }

        /// <summary>
        /// Template to use to send the data.
        /// </summary>
        public string TemplateName { get; set; }

        ///// <summary>
        ///// Data type ID to identity the type of the object inside data object.
        ///// </summary>
        //public string DataTypeId { get; set; }

        /// <summary>
        /// Data that need to be inserted in template.
        /// </summary>
        public string Data { get; set; }
    }
}
