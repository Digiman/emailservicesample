﻿using System.Collections.Generic;

namespace EmailService.Consumer.DTOs
{
    // TODO: implement DTO model to send emails

    /// <summary>
    /// DTO model to get from external systems.
    /// </summary>
    public sealed class EmailModelDTO
    {
        /// <summary>
        /// Email from - sender.
        /// </summary>
        public string FromEmail { get; set; }

        /// <summary>
        /// Name of the sender from.
        /// </summary>
        public string FromDisplayName { get; set; }

        /// <summary>
        /// List of emails to send to.
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// List of emails to copy (CC).
        /// </summary>
        public string Cc { get; set; }

        /// <summary>
        /// List of emails to copy (BCC).
        /// </summary>
        public string Bcc { get; set; }

        /// <summary>
        /// List of email to reply.
        /// </summary>
        public string Reply { get; set; }

        /// <summary>
        /// Mail subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Mail body.
        /// </summary>
        /// <remarks>Usually as HTML.</remarks>
        public string Body { get; set; }

        /// <summary>
        /// List of email attachments. 
        /// </summary>
        public List<EmailAttachmentDTO> Attachments { get; set; }

        public EmailModelDTO()
        {
            Attachments = new List<EmailAttachmentDTO>();
        }
    }
}
