﻿namespace EmailService.Consumer.Configuration
{
    // TODO: add more settings to configuration for using EmailService from consumer library (like api key, api version and other things.., if needed)

    /// <summary>
    /// Configuration for email service.
    /// </summary>
    public sealed class EmailServiceConfiguration
    {
        /// <summary>
        /// Base API Path to EmailService API.
        /// </summary>
        public string BaseApiPath { get; set; }

        /// <summary>
        /// Version for API (uses in URL routes).
        /// </summary>
        public string ApiVersion { get; set; }
    }
}
