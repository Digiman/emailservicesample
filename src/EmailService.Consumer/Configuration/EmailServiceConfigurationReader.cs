﻿using System.IO;
using EmailService.Consumer.Helpers;
using EmailService.Consumer.Interfaces;

namespace EmailService.Consumer.Configuration
{
    // TODO: implement logic to read configuration data from file
    // TODO: later it can be extended to not only read but save (create) configuration file

    /// <summary>
    /// Simple implementation of the logic to read configuration from the file for EmailService in consumer application.
    /// </summary>
    public sealed class EmailServiceConfigurationReader : IEmailServiceConfigurationReader
    {
        /// <summary>
        /// Load configuration from the file
        /// </summary>
        /// <param name="filename">Filename with configuration for email service to load on.</param>
        /// <returns>Returns configuration object with data from the file.</returns>
        public EmailServiceConfiguration Load(string filename)
        {
            if (string.IsNullOrEmpty(filename))
            {
                filename = Constants.DefaultConfigurationFileName;
            }

            var fileData = File.ReadAllText(filename);

            return SerializerHelper.DeserializeFromJson<EmailServiceConfiguration>(fileData);
        }
    }
}
