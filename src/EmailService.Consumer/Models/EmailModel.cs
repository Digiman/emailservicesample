﻿using System.Collections.Generic;

namespace EmailService.Consumer.Models
{
    /// <summary>
    /// Mail model with information how and whom to send email.
    /// External model to use in consumer applications.
    /// </summary>
    public sealed class EmailModel
    {
        /// <summary>
        /// Email from - sender.
        /// </summary>
        public string FromEmail { get; set; }

        /// <summary>
        /// Name of the sender from.
        /// </summary>
        public string FromDisplayName { get; set; }

        /// <summary>
        /// List of emails to send to.
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// List of emails to copy (CC).
        /// </summary>
        public string Cc { get; set; }

        /// <summary>
        /// List of emails to copy (BCC).
        /// </summary>
        public string Bcc { get; set; }

        /// <summary>
        /// List of email to reply.
        /// </summary>
        public string Reply { get; set; }

        /// <summary>
        /// Mail subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Mail body.
        /// </summary>
        /// <remarks>Usually as HTML.</remarks>
        public string Body { get; set; }

        /// <summary>
        /// List of email attachments. 
        /// </summary>
        public List<FileModel> Files { get; set; }

        public EmailModel()
        {
            Files = new List<FileModel>();
        }
    }
}
