﻿namespace EmailService.Consumer.Models
{
    /// <summary>
    /// Model to represent the file that need to be attached to email.
    /// External model to use in consumer applications.
    /// </summary>
    public sealed class FileModel
    {
        /// <summary>
        /// Filename (without path to file).
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Content of the file.
        /// </summary>
        public byte[] Content { get; set; }

        /// <summary>
        /// MIME type.
        /// </summary>
        public string ContentType { get; set; }
    }
}
