﻿namespace EmailService.Consumer.Models
{
    /// <summary>
    /// Main model to send email with template.
    /// External model to use in consumer applications.
    /// </summary>
    public sealed class EmailTemplateModel
    {
        /// <summary>
        /// Email model that describe basic information about sending email (recipients, subject and etc.).
        /// </summary>
        public EmailModel Model { get; set; }

        /// <summary>
        /// Name of the template to use in body of the email with data (Data property here).
        /// </summary>
        public string TemplateName { get; set; }

        /// <summary>
        /// Object with data for template.
        /// </summary>
        /// <remarks>
        /// To send to service will be serialized to JSON.
        /// </remarks>
        public object Data { get; set; }
    }
}
