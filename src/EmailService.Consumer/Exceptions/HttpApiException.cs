﻿using System;

namespace EmailService.Consumer.Exceptions
{
    /// <summary>
    /// Exception related to HTTP API calls in the library.
    /// </summary>
    public sealed class HttpApiException : Exception
    {
        public HttpApiException(string message) : base(message)
        {
        }

        public HttpApiException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
