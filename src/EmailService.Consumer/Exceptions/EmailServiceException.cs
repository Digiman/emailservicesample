﻿using System;

namespace EmailService.Consumer.Exceptions
{
    /// <summary>
    /// Custom exception to handle issues with email service.
    /// </summary>
    public sealed class EmailServiceException : Exception
    {
        public EmailServiceException(string message) : base(message)
        {
        }

        public EmailServiceException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
