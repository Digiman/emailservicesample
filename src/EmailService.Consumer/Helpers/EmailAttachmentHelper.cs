﻿using System;
using System.IO;
using EmailService.Consumer.DTOs;
using EmailService.Consumer.Models;

namespace EmailService.Consumer.Helpers
{
    // TODO: implement logic to work with email attachments with simple helper class

    /// <summary>
    /// Helper class to work with email attachments.
    /// </summary>
    public static class EmailAttachmentHelper
    {
        /// <summary>
        /// Create object with model for email attachment.
        /// </summary>
        /// <param name="fileModel">File model.</param>
        /// <returns>Returns object with email attachment model for service.</returns>
        public static EmailAttachmentDTO CreateEmailAttachment(FileModel fileModel)
        {
            var attachment = new EmailAttachmentDTO
            {
                Filename = Path.GetFileName(fileModel.FileName),
                FileData = Convert.ToBase64String(fileModel.Content),
                ContentType = fileModel.ContentType
            };

            return attachment;
        }
    }
}
