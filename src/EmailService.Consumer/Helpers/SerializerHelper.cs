﻿using Newtonsoft.Json;

namespace EmailService.Consumer.Helpers
{
    /// <summary>
    /// Simple class to work with data in JSON format.
    /// </summary>
    public static class SerializerHelper
    {
        /// <summary>
        /// Serialize data from object to JSON.
        /// </summary>
        /// <typeparam name="T">Data type.</typeparam>
        /// <param name="data">Data to serialize.</param>
        /// <returns>Returns JSON as string with serialized data.</returns>
        public static string SerializeToJson<T>(T data)
        {
            return JsonConvert.SerializeObject(data);
        }

        public static string SerializeToJson(object data)
        {
            return JsonConvert.SerializeObject(data);
        }

        /// <summary>
        /// Deserialize data from JSON to C# object (POCO).
        /// </summary>
        /// <typeparam name="T">Data type.</typeparam>
        /// <param name="data">String with data in JSON format.</param>
        /// <returns>Returns object with data from string.</returns>
        public static T DeserializeFromJson<T>(string data)
        {
            return JsonConvert.DeserializeObject<T>(data);
        }

        public static object DeserializeFromJson(string data)
        {
            return JsonConvert.DeserializeObject(data);
        }
    }
}
