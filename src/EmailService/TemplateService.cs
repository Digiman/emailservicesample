﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EmailService.Core.Data.Models;
using EmailService.Core.Template.Interfaces;
using EmailService.Interfaces;
using Microsoft.Extensions.Logging;

namespace EmailService
{
    // TODO: implement solution to work with templates. Maybe do something with Async methods also here (because templates loaded from files in sync)!

    /// <summary>
    /// Service to work with templates to access them from API.
    /// It helps to get available templates.
    /// </summary>
    public sealed class TemplateService : ITemplateService
    {
        private readonly ILogger<TemplateService> _logger;

        /// <summary>
        /// Loader for templates.
        /// </summary>
        private readonly ITemplateLoader _templateLoader;

        public TemplateService(ILogger<TemplateService> logger, ITemplateLoader templateLoader)
        {
            _logger = logger;
            _templateLoader = templateLoader;
        }

        public List<TemplateModel> GetAllTemplates()
        {
            _logger.LogInformation("Loading templates list...");

            var templates = _templateLoader.LoadTemplatesList();

            _logger.LogInformation("Templates list loaded");

            return templates;
        }

        public Task<List<TemplateModel>> GetAllTemplatesAsync()
        {
            return Task.FromResult(GetAllTemplates());
        }

        public Template GetTemplate(string templateName)
        {

            _logger.LogInformation($"Loading template for name: {templateName}");

            var template = _templateLoader.LoadTemplate(templateName);

            _logger.LogInformation("Template loaded");

            return template;
        }

        public Task<Template> GetTemplateAsync(string templateName)
        {
            return Task.FromResult(GetTemplate(templateName));
        }
    }
}
