﻿namespace EmailService.Interfaces
{
    public interface IEmailDataSerializer
    {
        string Serialize(object data);
        object Deserialize(string data);
    }
}
