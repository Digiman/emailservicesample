﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EmailService.Core.Data.Models;

namespace EmailService.Interfaces
{
    /// <summary>
    /// Service to work with templates to access them from API.
    /// It helps to get available templates.
    /// </summary>
    public interface ITemplateService
    {
        /// <summary>
        /// Get all available templates within application.
        /// </summary>
        /// <returns>Returns list with metadata for all available templates.</returns>
        List<TemplateModel> GetAllTemplates();

        /// <summary>
        /// Get all available templates within application.
        /// </summary>
        /// <returns>Returns list with metadata for all available templates.</returns>
        Task<List<TemplateModel>> GetAllTemplatesAsync();

        /// <summary>
        /// Get actual template by name.
        /// </summary>
        /// <param name="templateName">Template name (unique).</param>
        /// <returns>Returns template data with metadata.</returns>
        Template GetTemplate(string templateName);

        /// <summary>
        /// Get actual template by name.
        /// </summary>
        /// <param name="templateName">Template name (unique).</param>
        /// <returns>Returns template data with metadata.</returns>
        Task<Template> GetTemplateAsync(string templateName);
    }
}
