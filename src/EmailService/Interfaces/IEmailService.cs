﻿using System.Threading.Tasks;
using EmailService.Core.Data.Models;

namespace EmailService.Interfaces
{
    public interface IEmailService
    {
        /// <summary>
        /// Send email with predefined body in the model.
        /// </summary>
        /// <param name="model">Model with details how to send emails.</param>
        /// <returns>Returns some result on email sending.</returns>
        EmailResult SendEmail(EmailModel model);

        /// <summary>
        /// Send email with predefined body in the model.
        /// </summary>
        /// <param name="model">Model with details how to send emails.</param>
        /// <returns>Returns some result on email sending.</returns>
        Task<EmailResult> SendEmailAsync(EmailModel model);

        /// <summary>
        /// Send email with body generated from template.
        /// </summary>
        /// <typeparam name="T">Type of the object with data for template.</typeparam>
        /// <param name="model">Model with details how to send emails.</param>
        /// <param name="templateName">Template name (unique).</param>
        /// <param name="data">Data object with information for template.</param>
        /// <returns>Returns some result on email sending.</returns>
        EmailResult SendEmail<T>(EmailModel model, string templateName, T data);

        /// <summary>
        /// Send email with body generated from template.
        /// </summary>
        /// <typeparam name="T">Type of the object with data for template.</typeparam>
        /// <param name="model">Model with details how to send emails.</param>
        /// <param name="templateName">Template name (unique).</param>
        /// <param name="data">Data object with information for template.</param>
        /// <returns>Returns some result on email sending.</returns>
        Task<EmailResult> SendEmailAsync<T>(EmailModel model, string templateName, T data);
    }
}
