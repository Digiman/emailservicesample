﻿using System.Threading.Tasks;

namespace EmailService.Interfaces
{
    public interface IMailGenerator
    {
        /// <summary>
        /// Create email based on template.
        /// </summary>
        /// <typeparam name="T">Type of the data object with data to sue in template.</typeparam>
        /// <param name="templateName">Template name (unique name).</param>
        /// <param name="data">Data or email to render and insert to template.</param>
        /// <returns>Return rendered template with inserted data.</returns>
        string CreateEmail<T>(string templateName, T data);

        /// <summary>
        /// Create email based on template.
        /// </summary>
        /// <typeparam name="T">Type of the data object with data to sue in template.</typeparam>
        /// <param name="templateName">Template name (unique name).</param>
        /// <param name="data">Data or email to render and insert to template.</param>
        /// <returns>Return rendered template with inserted data.</returns>
        Task<string> CreateEmailAsync<T>(string templateName, T data);
    }
}
