﻿using System;
using System.Threading.Tasks;
using EmailService.Core.Data.Models;
using EmailService.Core.Template.Enums;
using EmailService.Core.Template.Interfaces;
using EmailService.Interfaces;
using Microsoft.Extensions.Logging;

namespace EmailService
{
    /// <summary>
    /// Main logic to generate emails in application.
    /// </summary>
    public sealed class MailGenerator : IMailGenerator
    {
        private readonly ILogger<MailGenerator> _logger;
        private readonly ITemplateEngineProvider _templateEngineProvider;
        private readonly ITemplateLoader _templateLoader;

        public MailGenerator(ILogger<MailGenerator> logger, ITemplateEngineProvider templateEngineProvider, ITemplateLoader templateLoader)
        {
            _logger = logger;
            _templateEngineProvider = templateEngineProvider;
            _templateLoader = templateLoader;
        }

        public string CreateEmail<T>(string templateName, T data)
        {
            // 1. Load template details and content.
            var template = _templateLoader.LoadTemplate(templateName);

            // if template not found do nothing
            if (template != null)
            {
                // 2. Identify engine for specific template.
                var engineType = GetTemplateEngineType(template);

                // if engine identified - use this engine, else - return empty string
                if (engineType != TemplateEngine.None)
                {
                    // 3. Get the engine to build email content.
                    var engine = _templateEngineProvider.GetTemplateEngine(engineType);

                    // 4. Generate mail body from template.
                    var result = engine.Generate(template, data);

                    _logger.LogInformation("Mail generated from template!");

                    return result;
                }
                else
                {
                    _logger.LogWarning("Template has non supported type!");
                }
            }
            else
            {
                _logger.LogError($"Template not found by name: ${templateName}.");
            }

            return String.Empty;
        }

        public async Task<string> CreateEmailAsync<T>(string templateName, T data)
        {
            // 1. Load template details and content.
            var template = _templateLoader.LoadTemplate(templateName);

            // if template not found do nothing
            if (template != null)
            {
                // 2. Identify engine for specific template.
                var engineType = GetTemplateEngineType(template);

                // if engine identified - use this engine, else - return empty string
                if (engineType != TemplateEngine.None)
                {
                    // 3. Get the engine to build email content.
                    var engine = _templateEngineProvider.GetTemplateEngine(engineType);

                    // 4. Generate mail body from template.
                    var result = await engine.GenerateAsync(template, data);

                    _logger.LogInformation("Mail generated from template!");

                    return result;
                }
                else
                {
                    _logger.LogWarning("Template has non supported type!");
                }
            }
            else
            {
                _logger.LogError($"Template not found by name: ${templateName}.");
            }

            return String.Empty;
        }

        #region Helpers.

        private TemplateEngine GetTemplateEngineType(Template template)
        {
            return GetTemplateEngine(template);
        }

        private TemplateEngine GetTemplateEngine(Template template)
        {
            switch (template.Type)
            {
                case "Razor":
                    return TemplateEngine.Razor;
                case "Markdown":
                    return TemplateEngine.Markdown;
                case "Hybrid":
                    return TemplateEngine.Hybrid;
                default:
                    return TemplateEngine.None;
            }
        }

        #endregion
    }
}
