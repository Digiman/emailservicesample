﻿using System;
using System.Threading.Tasks;
using EmailService.Core.Audit.Interfaces;
using EmailService.Core.Common.Interfaces;
using EmailService.Core.Data.Enums;
using EmailService.Core.Data.Models;
using EmailService.Core.Email.Enums;
using EmailService.Core.Email.Interfaces;
using EmailService.Interfaces;
using Microsoft.Extensions.Logging;

namespace EmailService
{
    /// <summary>
    /// Logic to actual send emails and use main generator based on some supported template engines. 
    /// </summary>
    public sealed class EmailService : IEmailService
    {
        /// <summary>
        /// Logger for current class.
        /// </summary>
        private readonly ILogger<EmailService> _logger;

        /// <summary>
        /// Provider to use configuration settings.
        /// </summary>
        private readonly IApplicationConfigurationProvider _applicationConfigurationProvider;

        /// <summary>
        /// Mail generator to create body from template and data.
        /// </summary>
        private readonly IMailGenerator _mailGenerator;
        
        /// <summary>
        /// Provider to use provider to send emails.
        /// </summary>
        private readonly IEmailSenderProvider _emailSenderProvider;

        /// <summary>
        /// Logic to use audit for sending emails.
        /// </summary>
        private readonly IEmailAudit _emailAudit;

        /// <summary>
        /// Validator for email models.
        /// </summary>
        private readonly IModelsValidator _modelsValidator;

        public EmailService(IMailGenerator mailGenerator, ILogger<EmailService> logger,
            IApplicationConfigurationProvider applicationConfigurationProvider,
            IEmailSenderProvider emailSenderProvider, IEmailAudit emailAudit, 
            IModelsValidator modelsValidator)
        {
            _mailGenerator = mailGenerator;
            _logger = logger;
            _applicationConfigurationProvider = applicationConfigurationProvider;
            _emailSenderProvider = emailSenderProvider;
            _emailAudit = emailAudit;
            _modelsValidator = modelsValidator;
        }

        #region Send simple emails.

        /// <summary>
        /// Send email with predefined body in the model.
        /// </summary>
        /// <param name="model">Model with details how to send emails.</param>
        /// <returns>Returns some result on email sending.</returns>
        public EmailResult SendEmail(EmailModel model)
        {
            var result = new EmailResult {Status = ResultStatus.None};

            try
            {
                _logger.LogInformation("Sending email...");

                // 1. Validate model to correct data.
                var validationResult = ValidateEmailModel(model);

                if (validationResult.Status == ValidationStatus.Valid)
                {
                    // 2. Send email.
                    SendEmailMessage(model);

                    // 3. Save email audit.
                    CreateEmailAudit(model, null);

                    _logger.LogInformation("Email sent");

                    result.Status = ResultStatus.Success;
                }
                else
                {
                    _logger.LogError("Model is not valid!");

                    // update status and save all validation errors
                    result.Status = ResultStatus.Error;
                    result.ErrorDetails = validationResult.Errors;
                }
            }
            catch (Exception ex)
            {
                result.Status = ResultStatus.Error;
                result.ErrorMessage = ex.Message;

                _logger.LogError("Error: " + ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Send email with predefined body in the model.
        /// </summary>
        /// <param name="model">Model with details how to send emails.</param>
        /// <returns>Returns some result on email sending.</returns>
        public async Task<EmailResult> SendEmailAsync(EmailModel model)
        {
            var result = new EmailResult {Status = ResultStatus.None};

            try
            {
                _logger.LogInformation("Sending email...");

                // 1. Validate model to correct data.
                var validationResult = ValidateEmailModel(model);

                if (validationResult.Status == ValidationStatus.Valid)
                {
                    // 2. Send email.
                    await SendEmailMessageAsync(model);

                    // 3. Save email audit.
                    await CreateEmailAuditAsync(model, null);

                    _logger.LogInformation("Email sent");

                    result.Status = ResultStatus.Success;
                }
                else
                {
                    _logger.LogError("Model is not valid!");

                    // update status and save all validation errors
                    result.Status = ResultStatus.Error;
                    result.ErrorDetails = validationResult.Errors;
                }
            }
            catch (Exception ex)
            {
                result.Status = ResultStatus.Error;
                result.ErrorMessage = ex.Message;

                _logger.LogError("Error: " + ex.Message);
            }

            return result;
        }

        #endregion

        #region Send emails based on templates.

        /// <summary>
        /// Send email with body generated from template.
        /// </summary>
        /// <typeparam name="T">Type of the object with data for template.</typeparam>
        /// <param name="model">Model with details how to send emails.</param>
        /// <param name="templateName">Template name (unique).</param>
        /// <param name="data">Data object with information for template.</param>
        /// <returns>Returns some result on email sending.</returns>
        public EmailResult SendEmail<T>(EmailModel model, string templateName, T data)
        {
            var result = new EmailResult {Status = ResultStatus.None};

            try
            {
                _logger.LogInformation("Sending email...");

                // 1. Validate model to correct data.
                var validationResult = ValidateEmailModel(model);

                if (validationResult.Status == ValidationStatus.Valid)
                {
                    // 2. Create email from template.
                    var mailBody = _mailGenerator.CreateEmail(templateName, data);

                    // 3. Update email model.
                    model.Body = mailBody;

                    // 4. Send email.
                    SendEmailMessage(model);

                    // 5. Save email audit.
                    CreateEmailAudit(model, templateName);

                    _logger.LogInformation("Email sent");

                    result.Status = ResultStatus.Success;
                }
                else
                {
                    _logger.LogError("Model is not valid!");

                    // update status and save all validation errors
                    result.Status = ResultStatus.Error;
                    result.ErrorDetails = validationResult.Errors;
                }
            }
            catch (Exception ex)
            {
                result.Status = ResultStatus.Error;
                result.ErrorMessage = ex.Message;

                _logger.LogError("Error: " + ex.Message);
            }

            return result;
        }

        /// <summary>
        /// Send email with body generated from template.
        /// </summary>
        /// <typeparam name="T">Type of the object with data for template.</typeparam>
        /// <param name="model">Model with details how to send emails.</param>
        /// <param name="templateName">Template name (unique).</param>
        /// <param name="data">Data object with information for template.</param>
        /// <returns>Returns some result on email sending.</returns>
        public async Task<EmailResult> SendEmailAsync<T>(EmailModel model, string templateName, T data)
        {
            var result = new EmailResult {Status = ResultStatus.None};

            try
            {
                _logger.LogInformation("Sending email...");

                // 1. Validate model to correct data.
                var validationResult = ValidateEmailModel(model);

                if (validationResult.Status == ValidationStatus.Valid)
                {
                    // 2. Create email from template.
                    var mailBody = await _mailGenerator.CreateEmailAsync(templateName, data);

                    // 3. Update email model.
                    model.Body = mailBody;

                    // 4. Send email.
                    await SendEmailMessageAsync(model);

                    // 5. Save email audit.
                    await CreateEmailAuditAsync(model, templateName);

                    _logger.LogInformation("Email sent");

                    result.Status = ResultStatus.Success;
                }
                else
                {
                    _logger.LogError("Model is not valid!");

                    // update status and save all validation errors
                    result.Status = ResultStatus.Error;
                    result.ErrorMessage = "Email can't be send! Model is not valid.";
                    result.ErrorDetails = validationResult.Errors;
                }
            }
            catch (Exception ex)
            {
                result.Status = ResultStatus.Error;
                result.ErrorMessage = ex.Message;

                _logger.LogError("Error: " + ex.Message);
            }

            return result;
        }

        #endregion

        #region Helpers.

        /// <summary>
        /// Do validation for email model.
        /// </summary>
        /// <param name="model">Model to validate.</param>
        /// <returns>Returns validation results.</returns>
        private EmailModelValidationResult ValidateEmailModel(EmailModel model)
        {
            _logger.LogInformation("Validate email model....");

            var result = _modelsValidator.ValidateEmailModel(model);

            _logger.LogInformation("Validation for email models finished");

            return result;
        }

        #region Send emails.

        /// <summary>
        /// Send email using available provider.
        /// </summary>
        /// <param name="model">Email data to send.</param>
        private void SendEmailMessage(EmailModel model)
        {
            var emailSender = GetEmailSender();

            emailSender.SendEmail(model);
        }

        /// <summary>
        /// Send email using available provider.
        /// </summary>
        /// <param name="model">Email data to send.</param>
        private async Task SendEmailMessageAsync(EmailModel model)
        {
            var emailSender = GetEmailSender();

            await emailSender.SendEmailAsync(model);
        }

        #endregion

        #region Work with email audit.

        private void CreateEmailAudit(EmailModel model, string templateName)
        {
            _logger.LogInformation("Creating email audit...");

            // TODO: add audit here
            var auditModel = new EmailAuditModel();
            
            _emailAudit.SaveEmailAudit(auditModel);

            _logger.LogInformation("Email audit created");
        }

        private async Task CreateEmailAuditAsync(EmailModel model, string templateName)
        {
            _logger.LogInformation("Creating email audit...");

            // TODO: add audit here
            var auditModel = new EmailAuditModel();

            await _emailAudit.SaveEmailAuditAsync(auditModel);

            _logger.LogInformation("Email audit created");
        }

        #endregion

        /// <summary>
        /// Get actual instance of the email sender to send email based on provider from configuration.
        /// </summary>
        /// <returns>Returns instance of the email sender.</returns>
        private IEmailSender GetEmailSender()
        {
            var senderType = GetEmailSenderType();

            var emailSender = _emailSenderProvider.GetEmailSender(senderType);

            return emailSender;
        }

        /// <summary>
        /// Identify provider type from configuration to use to send emails.
        /// </summary>
        /// <returns>Returns email sender/provider type.</returns>
        private EmailSenderType GetEmailSenderType()
        {
            var senderType = _applicationConfigurationProvider.GetEmailServiceConfiguration().MailProvider;
            switch (senderType)
            {
                case "Generic":
                    return EmailSenderType.Generic;
                case "SendGrid":
                    return EmailSenderType.SendGrid;
                default:
                    return EmailSenderType.Generic;
            }
        }

        #endregion
    }
}
