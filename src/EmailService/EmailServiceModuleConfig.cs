﻿using EmailService.Interfaces;
using EmailService.Serializers;
using Microsoft.Extensions.DependencyInjection;

namespace EmailService
{
    /// <summary>
    /// Configuration for DI for Email Service module.
    /// </summary>
    public static class EmailServiceModuleConfig
    {
        /// <summary>
        /// Configure all dependencies for Email Service module.
        /// </summary>
        /// <param name="serviceCollection">Services in container.</param>
        private static void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IEmailService, EmailService>();

            serviceCollection.AddSingleton<IMailGenerator, MailGenerator>();

            serviceCollection.AddSingleton<ITemplateService, TemplateService>();

            serviceCollection.AddSingleton<IEmailDataSerializer, EmailDataSerializer>();
        }

        /// <summary>
        /// Configure DI for Email Service module components.
        /// </summary>
        /// <param name="serviceCollection">Services in container.</param>
        public static void InitializeEmailServiceModule(this IServiceCollection serviceCollection)
        {
            Configure(serviceCollection);
        }
    }
}
