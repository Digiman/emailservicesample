﻿using EmailService.Core.Common.Helpers;
using EmailService.Interfaces;

namespace EmailService.Serializers
{
    // TODO: maybe later here can be implemented more powerful serializer based in data types...

    // NOTE: ideally here we can use another specific JSON serializer or other type of serializer (base64 and etc.) that allow to send data in text form in POST request to API

    /// <summary>
    /// Simple implementation for email data serializers to send data for template via API.
    /// </summary>
    public sealed class EmailDataSerializer : IEmailDataSerializer
    {
        public string Serialize(object data)
        {
            return SerializerHelper.SerializeToJson(data);
        }

        public object Deserialize(string data)
        {
            return SerializerHelper.DeserializeFromJson(data);
        }
    }
}
