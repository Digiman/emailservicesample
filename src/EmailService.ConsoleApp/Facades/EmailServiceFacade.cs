﻿using System.Threading.Tasks;
using EmailService.ConsoleApp.Interfaces;
using EmailService.Core.Data.Enums;
using EmailService.Core.Data.MailData;
using EmailService.Core.Data.Models;
using EmailService.Interfaces;
using Microsoft.Extensions.Logging;

namespace EmailService.ConsoleApp.Facades
{
    // TODO: 1. implement and text bases tet methods to check how template engines and email sender works
    // TODO: 2. add more logic related to run emails from console (for suture when we can use console tool to send emails from any local apps)

    /// <summary>
    /// Simple facade to test and use main base logic in application from Console application.
    /// </summary>
    public sealed class EmailServiceFacade : IEmailServiceFacade
    {
        private readonly ILogger<EmailServiceFacade> _logger;

        private const string DefaultTo = "akukharenko@exadel.com";
        private const string DefaultFrom = "digiman89@gmail.com";
        private const string DefaultDisplayName = "Andrey Kukharenko";

        private string DisplayName
        {
            get { return "Andrey Kukharenko" ?? DefaultDisplayName; }
        }

        private string From
        {
            get { return "digiman89@gmail.com" ?? DefaultFrom; }
        }

        private string To
        {
            get { return /*"Papercut@user.com" ??*/ DefaultTo; }
        }

        /// <summary>
        /// Main entry point for using email sending functionality.
        /// </summary>
        private readonly IEmailService _emailService;

        public EmailServiceFacade(ILogger<EmailServiceFacade> logger, IEmailService emailService)
        {
            _logger = logger;
            _emailService = emailService;
        }

        #region Methods to send emails.

        public async Task SendTestHybridEmailAsync()
        {
            var model = new EmailModel
            {
                FromDisplayName = DisplayName,
                FromEmail = From,
                To = To,
                Subject = "Test email from new EmailService [CoreConsoleApp]"
            };

            var data = new TestMailDataModel
            {
                Name = "test name",
                Description = "test description"
            };

            var result = await _emailService.SendEmailAsync(model, "TestHybrid", data);

            LogResult(result);
        }

        public async Task SendTestRazorEmailAsync()
        {
            var model = new EmailModel
            {
                FromDisplayName = DisplayName,
                FromEmail = From,
                To = To,
                Subject = "Test email from new EmailService [CoreConsoleApp]"
            };

            var data = new TestMailDataModel
            {
                Name = "test name",
                Description = "test description"
            };

            var result = await _emailService.SendEmailAsync(model, "TestRazor", data);

            LogResult(result);
        }

        public async Task SendTestMarkdownEmailAsync()
        {
            var model = new EmailModel
            {
                FromDisplayName = DisplayName,
                FromEmail = From,
                To = To,
                Subject = "Test email from new EmailService [CoreConsoleApp]"
            };

            var data = new TestMailDataModel
            {
                Name = "test name",
                Description = "test description"
            };

            var result = await _emailService.SendEmailAsync(model, "TestMarkdown", data);

            LogResult(result);
        }

        #endregion

        #region Helpers.

        private void LogResult(EmailResult result)
        {
            if (result.Status == ResultStatus.Error)
            {
                _logger.LogError($"FACADE -> Something happened with sending email. Error: {result.ErrorMessage}");
            }
            else
            {
                _logger.LogInformation("FACADE -> Email successfully sent!");
            }
        }

        #endregion
    }
}
