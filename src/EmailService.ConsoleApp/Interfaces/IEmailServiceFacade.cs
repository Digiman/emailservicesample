﻿using System.Threading.Tasks;

namespace EmailService.ConsoleApp.Interfaces
{
    public interface IEmailServiceFacade
    {
        Task SendTestHybridEmailAsync();
        Task SendTestRazorEmailAsync();
        Task SendTestMarkdownEmailAsync();
    }
}
