﻿using System;
using System.Threading.Tasks;
using EmailService.ConsoleApp.Interfaces;
using Microsoft.Extensions.Logging;

namespace EmailService.ConsoleApp
{
    /// <summary>
    /// Entry point of the application after running and configuring DI container.
    /// </summary>
    public sealed class Application
    {
        private readonly ILogger<Application> _logger;
        private readonly IEmailServiceFacade _emailServiceFacade;

        public Application(ILogger<Application> logger, IEmailServiceFacade emailServiceFacade)
        {
            _logger = logger;
            _emailServiceFacade = emailServiceFacade;
        }

        /// <summary>
        /// Run the application main logic with using DI.
        /// </summary>
        /// <param name="args">Arguments from command line.</param>
        public async Task RunAsync(string[] args)
        {
            // TODO: add actual code here to run application logic!
            try
            {
                _logger.LogInformation("Application started...");

                //------------------------------------------------

                // TODO: Parse command line arguments to Options class. - move to starter class!
                //ParseArguments(args);
                
                await ExecuteActionAsync();

                //------------------------------------------------

                _logger.LogInformation("Application finished...");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, ex.Message);
                //throw;
            }
        }

        /*private void ParseArguments(string[] args)
        {
            // create a generic parser for the ApplicationArguments type
            var p = new FluentCommandLineParser<ApplicationArguments>();

            // specify which property the value will be assigned too.
            p.Setup(arg => arg.TemplateName)
                .As('t', "template") // define the short and long option name
                .Required(); // using the standard fluent Api to declare this Option as required.

            var result = p.Parse(args);

            if (result.HasErrors == false)
            {

            }
        }*/

        /// <summary>
        /// Very simple method to call some test actions.
        /// </summary>
        private async Task ExecuteActionAsync()
        {
            // 1. Test 1.
            await _emailServiceFacade.SendTestHybridEmailAsync();

            // 2. Test 2.
            await _emailServiceFacade.SendTestMarkdownEmailAsync();

            // 3. Test 3.
            await _emailServiceFacade.SendTestRazorEmailAsync();
        }
    }
}
