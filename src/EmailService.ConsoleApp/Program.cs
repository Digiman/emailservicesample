﻿using System;
using System.IO;
using System.Threading.Tasks;
using EmailService.ConsoleApp.Facades;
using EmailService.ConsoleApp.Interfaces;
using EmailService.Core.Audit;
using EmailService.Core.Common;
using EmailService.Core.Common.Configuration;
using EmailService.Core.Email;
using EmailService.Core.Template;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;

namespace EmailService.ConsoleApp
{
    /// <summary>
    /// Main starter class in the application to run the console program.
    /// </summary>
    static class Program
    {
        /// <summary>
        /// Configuration for application from JSON files.
        /// </summary>
        private static IConfiguration Configuration { get; set; }

        /// <summary>
        /// Service provider to configure DI container.
        /// </summary>
        private static ServiceProvider ServiceProvider { get; set; }

        /// <summary>
        /// Main method - entry point for console application.
        /// </summary>
        /// <param name="args">Arguments from command line.</param>
        static void Main(string[] args)
        {
            try
            {
                // TODO: find the most proper way to run application in async way from sync main method
                MainInternalAsync(args).GetAwaiter().GetResult();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Error: {ex.Message}");
            }

            // add simple finish for application
            Console.WriteLine("------------------------------------------------------");
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }

        /// <summary>
        /// Main logic to configure application to start. looks the same as Startup.cs.
        /// </summary>
        /// <param name="args">Arguments from command line.</param>
        private static async Task MainInternalAsync(string[] args)
        {
            // Get environment settings
            string env = Environment.GetEnvironmentVariable("NETCORE_ENVIRONMENT");

            if (string.IsNullOrWhiteSpace(env))
            {
                env = "dev";
            }

            // Build and load settings from json files
            var builder = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env}.json", optional: false, reloadOnChange: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();

            ConfigureLogger();

            // Create a service collection and configure our dependencies
            var serviceCollection = new ServiceCollection();
            ConfigureServices(serviceCollection);

            // Build the our IServiceProvider and set our static reference to it
            ServiceProvider = serviceCollection.BuildServiceProvider();

            // Enter the application.. (run!)
            await ServiceProvider.GetService<Application>().RunAsync(args);
        }

        /// <summary>
        /// Configure all services for DI.
        /// </summary>
        /// <param name="services">Service collection.</param>
        private static void ConfigureServices(IServiceCollection services)
        {
            services.AddSingleton<IEmailServiceFacade, EmailServiceFacade>();
            
            // Make configuration settings available.
            services.Configure<ApplicationConfiguration>((s) => Configuration.GetSection("application"));
            services.Configure<EmailServiceConfiguration>((s) => Configuration.GetSection("emailService"));
            services.AddSingleton<IConfiguration>(Configuration);

            // Add all related modules here.
            services.InitializeCommonModule();
            services.InitializeTemplateModule();
            services.InitializeEmailModule();
            services.InitializeEmailServiceModule();
            services.InitializeEmailAuditModule();

            // Add logging  
            services.AddSingleton(new LoggerFactory().AddSerilog());
            services.AddLogging();

            // Add Application 
            services.AddTransient<Application>();
        }

        /// <summary>
        /// Configure Serilog logger in the application.
        /// Will use settings from configuration file.
        /// </summary>
        private static void ConfigureLogger()
        {
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(Configuration)
                .CreateLogger();
        }
    }
}
