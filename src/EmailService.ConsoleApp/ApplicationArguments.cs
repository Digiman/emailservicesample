﻿namespace EmailService.ConsoleApp
{
    public sealed class ApplicationArguments
    {
        public string TemplateName { get; set; }
    }
}
