﻿using EmailService.Core.Common.Helpers;
using EmailService.Core.Template.Interfaces;

namespace EmailService.Core.Template.Serializers
{
    public sealed class TemplatesIndexSerializer : ITemplatesIndexSerializer
    {
        public string Serialize<T>(T data)
        {
            return SerializerHelper.SerializeToJson(data);
        }

        public T Deserialize<T>(string data)
        {
            return SerializerHelper.DeserializeFromJson<T>(data);
        }
    }
}
