﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using EmailService.Core.Common.Interfaces;
using EmailService.Core.Data.Models;
using EmailService.Core.Template.Interfaces;
using Microsoft.Extensions.Logging;

namespace EmailService.Core.Template.Loaders
{
    // TODO: Maybe later create loaded from file system or from DB and etc.
    // TODO: add here additional logic to handle possible exceptions!

    /// <summary>
    /// Generic loader to load templates from different sources. 
    /// </summary>
    /// <remarks>
    /// Here is implementer class with logic to read data from files locally.
    /// It can be implemented for different places (DB, network) to find and read information about templates.
    /// </remarks>
    public sealed class TemplateLoader : ITemplateLoader
    {
        private readonly ILogger<TemplateLoader> _logger;

        /// <summary>
        /// Application settings from the files.
        /// </summary>
        private readonly IApplicationConfigurationProvider _applicationConfigurationProvider;

        /// <summary>
        /// Simple file manager to work files locally.
        /// </summary>
        private readonly IFileManager _fileManager;
        
        /// <summary>
        /// Serializer for index file with templates metadata.
        /// </summary>
        private readonly ITemplatesIndexSerializer _templatesIndexSerializer;
        
        public TemplateLoader(IFileManager fileManager, ILogger<TemplateLoader> logger,
            ITemplatesIndexSerializer templatesIndexSerializer,
            IApplicationConfigurationProvider applicationConfigurationProvider)
        {
            _fileManager = fileManager;
            _logger = logger;
            _templatesIndexSerializer = templatesIndexSerializer;
            _applicationConfigurationProvider = applicationConfigurationProvider;
        }

        /// <summary>
        /// Load list of templates available in the application from index file.
        /// </summary>
        /// <returns>Returns loaded metadata about templates.</returns>
        public List<TemplateModel> LoadTemplatesList()
        {
            var fileData = _fileManager.LoadFileToString(TemplatesIndexFile);
            var templates = _templatesIndexSerializer.Deserialize<List<TemplateModel>>(fileData);

            return templates;
        }

        /// <summary>
        /// Load template data and metadata.
        /// </summary>
        /// <param name="templateName">Template name (unique value).</param>
        /// <returns>Returns loaded template and </returns>
        public Data.Models.Template LoadTemplate(string templateName)
        {
            _logger.LogInformation($"Loading template by name: {templateName}...");

            var templates = LoadTemplatesList();

            var templateModel = templates.FirstOrDefault(x => x.Name == templateName);
            Data.Models.Template template = null;
            if (templateModel != null)
            {
                // choose by default content from fields in the metadata model
                var content = templateModel.Content;

                // load template content from the file
                if (templateModel.IsTemplateInFile)
                {
                    var templateFile = GetFullTemplatePath(TemplatesPath, templateModel.TemplateFile);
                    content = _fileManager.LoadFileToString(templateFile);
                }
                
                template = new Data.Models.Template
                {
                    Name = templateName,
                    Description = templateModel.Description,
                    Type = templateModel.Type,
                    Content = content
                };

                _logger.LogInformation("Template loaded successfully!");
            }
            else
            {
                _logger.LogWarning("Template not found!");
            }

            return template;
        }

        #region Helpers.

        private string TemplatesIndexFile
        {
            get { return _applicationConfigurationProvider.GetEmailServiceConfiguration().TemplatesIndexFile; }
        }

        private string TemplatesPath
        {
            get { return _applicationConfigurationProvider.GetEmailServiceConfiguration().TemplatesPath; }
        }

        private string GetFullTemplatePath(string path, string fileName)
        {
            return Path.Combine(path, fileName);
        }

        #endregion
    }
}
