﻿using EmailService.Core.Template.Generators;
using EmailService.Core.Template.Interfaces;
using EmailService.Core.Template.Loaders;
using EmailService.Core.Template.Serializers;
using Microsoft.Extensions.DependencyInjection;

namespace EmailService.Core.Template
{
    /// <summary>
    /// Configuration for services inside Template module/library.
    /// </summary>
    public static class TemplateModuleConfig
    {
        /// <summary>
        /// Configure all dependencies for Template module.
        /// </summary>
        /// <param name="serviceCollection">Services in container.</param>
        private static void Configure(IServiceCollection serviceCollection)
        {
            // register generators
            serviceCollection.AddSingleton<IMarkdownTemplateGenerator, MarkdownTemplateGenerator>();
            serviceCollection.AddSingleton<IRazorTemplateGenerator, RazorTemplateGenerator>();

            // register providers
            serviceCollection.AddSingleton<ITemplateEngineProvider, TemplateEngineProvider>();

            // serializers
            serviceCollection.AddSingleton<ITemplatesIndexSerializer, TemplatesIndexSerializer>();

            // loaders
            serviceCollection.AddSingleton<ITemplateLoader, TemplateLoader>();
        }

        /// <summary>
        /// Configure DI for Template module components.
        /// </summary>
        /// <param name="serviceCollection">Services in container.</param>
        public static void InitializeTemplateModule(this IServiceCollection serviceCollection)
        {
            Configure(serviceCollection);
        }
    }
}
