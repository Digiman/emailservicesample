﻿namespace EmailService.Core.Template.Enums
{
    /// <summary>
    /// Types os available template engines within the application.
    /// </summary>
    public enum TemplateEngine
    {
        /// <summary>
        /// No any engines will be used. Default value.
        /// </summary>
        None = 0,

        /// <summary>
        /// Markdown.
        /// </summary>
        Markdown = 1,

        /// <summary>
        /// Razor engine only.
        /// </summary>
        Razor = 2,

        /// <summary>
        /// Markdown + Razor.
        /// </summary>
        Hybrid = 3
    }
}
