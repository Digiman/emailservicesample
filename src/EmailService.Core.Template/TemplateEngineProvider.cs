﻿using System;
using System.Collections.Generic;
using EmailService.Core.Template.Engines;
using EmailService.Core.Template.Enums;
using EmailService.Core.Template.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace EmailService.Core.Template
{
    /// <summary>
    /// Provider to allow select right template engine to work with templates.
    /// </summary>
    public sealed class TemplateEngineProvider : ITemplateEngineProvider
    {
        private readonly IServiceProvider _serviceProvider;

        private readonly IDictionary<TemplateEngine, Type> _supportedEngines = new Dictionary<TemplateEngine, Type>()
        {
            {TemplateEngine.Hybrid, typeof(HybridTemplateEngine)},
            {TemplateEngine.Razor, typeof(RazorTemplateEngine)},
            {TemplateEngine.Markdown, typeof(MarkdownTemplateEngine)}
        };

        public TemplateEngineProvider(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        /// <summary>
        /// Get instance of the template engine that will generate email content with data.
        /// </summary>
        /// <param name="engineType">Engine type for specific template.</param>
        /// <returns>Returns instance of the active template engine.</returns>
        public ITemplateEngine GetTemplateEngine(TemplateEngine engineType)
        {
            var type = _supportedEngines[engineType];
            return (ITemplateEngine) ActivatorUtilities.GetServiceOrCreateInstance(_serviceProvider, type);
        }
    }
}
