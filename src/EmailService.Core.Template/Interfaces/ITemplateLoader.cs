﻿using System.Collections.Generic;
using EmailService.Core.Data.Models;

namespace EmailService.Core.Template.Interfaces
{
    public interface ITemplateLoader
    {
        /// <summary>
        /// Load list of templates available in the application from index file.
        /// </summary>
        /// <returns>Returns loaded metadata about templates.</returns>
        List<TemplateModel> LoadTemplatesList();

        /// <summary>
        /// Load template data and metadata.
        /// </summary>
        /// <param name="templateName">Template name (unique value).</param>
        /// <returns>Returns loaded template and </returns>
        Data.Models.Template LoadTemplate(string templateName);
    }
}
