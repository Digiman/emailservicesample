﻿namespace EmailService.Core.Template.Interfaces
{
    /// <summary>
    /// Serializer for working with Index file with templates descriptions.
    /// </summary>
    public interface ITemplatesIndexSerializer
    {
        string Serialize<T>(T data);
        T Deserialize<T>(string data);
    }
}
