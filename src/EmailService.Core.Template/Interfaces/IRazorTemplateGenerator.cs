﻿using System.Threading.Tasks;

namespace EmailService.Core.Template.Interfaces
{
    public interface IRazorTemplateGenerator
    {
        /// <summary>
        /// Convert Razor file (cshtml) to HTML with insertion of the data.
        /// </summary>
        /// <param name="templateKey">Unique template key.</param>
        /// <param name="model">Model with data for email.</param>
        /// <param name="template">Template content.</param>
        /// <returns>Returns generated HTML page with data from model.</returns>
        Task<string> BuildTemplateAsync(string templateKey, object model, string template);
    }
}
