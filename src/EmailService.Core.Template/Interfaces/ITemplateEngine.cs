﻿using System.Threading.Tasks;

namespace EmailService.Core.Template.Interfaces
{
    /// <summary>
    /// Template engine to proceed with templates.
    /// </summary>
    public interface ITemplateEngine
    {
        /// <summary>
        /// Generate HTML content from template.
        /// </summary>
        /// <param name="template">Template data.</param>
        /// <param name="dataModel">Model with data to put into template.</param>
        /// <returns>Returns HTML generated text.</returns>
        string Generate(Data.Models.Template template, object dataModel);

        /// <summary>
        /// Generate HTML content from template.
        /// </summary>
        /// <param name="template">Template data.</param>
        /// <param name="dataModel">Model with data to put into template.</param>
        /// <returns>Returns HTML generated text.</returns>
        Task<string> GenerateAsync(Data.Models.Template template, object dataModel);
    }
}
