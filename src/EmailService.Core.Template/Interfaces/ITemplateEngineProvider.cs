﻿using EmailService.Core.Template.Enums;

namespace EmailService.Core.Template.Interfaces
{
    public interface ITemplateEngineProvider
    {
        /// <summary>
        /// Get instance of the template engine that will generate email content with data.
        /// </summary>
        /// <param name="engineType">Engine type for specific template.</param>
        /// <returns>Returns instance of the active template engine.</returns>
        ITemplateEngine GetTemplateEngine(TemplateEngine engineType);
    }
}
