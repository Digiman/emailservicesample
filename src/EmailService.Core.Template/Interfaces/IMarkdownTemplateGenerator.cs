﻿using System.Threading.Tasks;

namespace EmailService.Core.Template.Interfaces
{
    public interface IMarkdownTemplateGenerator
    {
        /// <summary>
        /// Convert template from Markdown to HTML.
        /// </summary>
        /// <param name="template">Actual text with Markdown format to render.</param>
        /// <returns>?Returns HTML view after render.</returns>
        string BuildTemplate(string template);

        /// <summary>
        /// Convert template from Markdown to HTML.
        /// </summary>
        /// <param name="template">Actual text with Markdown format to render.</param>
        /// <returns>?Returns HTML view after render.</returns>
        Task<string> BuildTemplateAsync(string template);
    }
}
