﻿using System.Threading.Tasks;
using EmailService.Core.Template.Interfaces;
using RazorLight;

namespace EmailService.Core.Template.Generators
{
    // TODO: implement here specific logic to generate templates based on Razor engine
    
    // TODO: see here - https://github.com/toddams/RazorLight
    
    /// <summary>
    /// Generator for specific logic to handle Razor templates.
    /// </summary>
    public sealed class RazorTemplateGenerator : IRazorTemplateGenerator
    {
        /// <summary>
        /// Convert Razor file (cshtml) to HTML with insertion of the data.
        /// </summary>
        /// <param name="templateKey">Unique template key.</param>
        /// <param name="model">Model with data for email.</param>
        /// <param name="template">Template content.</param>
        /// <returns>Returns generated HTML page with data from model.</returns>
        public async Task<string> BuildTemplateAsync(string templateKey, object model, string template)
        {
            var engine = new RazorLightEngineBuilder()
                .UseMemoryCachingProvider()
                .Build();

            string result;

            var cacheResult = engine.TemplateCache.RetrieveTemplate(templateKey);
            if (cacheResult.Success)
            {
                result = await engine.RenderTemplateAsync(cacheResult.Template.TemplatePageFactory(), model);
            }
            else
            {
                result = await engine.CompileRenderAsync(templateKey, template, model);
            }

            return result;
        }
    }
}
