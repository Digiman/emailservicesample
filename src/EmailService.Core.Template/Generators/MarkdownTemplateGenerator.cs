﻿using System.Threading.Tasks;
using EmailService.Core.Template.Interfaces;

namespace EmailService.Core.Template.Generators
{
    // TODO: implement here specific logic to generate templates based on Markdown syntax

    // see here: https://github.com/lunet-io/markdig

    /// <summary>
    /// Implementation for generator to transform Markdown templates to HTML.
    /// </summary>
    public sealed class MarkdownTemplateGenerator : IMarkdownTemplateGenerator
    {
        /// <summary>
        /// Convert template from Markdown to HTML.
        /// </summary>
        /// <param name="template">Actual text with Markdown format to render.</param>
        /// <returns>?Returns HTML view after render.</returns>
        public string BuildTemplate(string template)
        {
            // TODO: test this method and maybe improve by additional configuration

            var result = Markdig.Markdown.ToHtml(template);

            return result;
        }

        /// <summary>
        /// Convert template from Markdown to HTML.
        /// </summary>
        /// <param name="template">Actual text with Markdown format to render.</param>
        /// <returns>?Returns HTML view after render.</returns>
        public Task<string> BuildTemplateAsync(string template)
        {
            // TODO: test this method and maybe improve by additional configuration

            var result = Markdig.Markdown.ToHtml(template);

            return Task.FromResult(result);
        }
    }
}
