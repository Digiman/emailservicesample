﻿using System.Threading.Tasks;
using EmailService.Core.Template.Interfaces;

namespace EmailService.Core.Template.Engines
{
    /// <summary>
    /// Engine to proceed files in Markdown format.
    /// Note that here without models! Only convert template from Markdown to HTML!
    /// </summary>
    public sealed class MarkdownTemplateEngine : ITemplateEngine
    {
        private readonly IMarkdownTemplateGenerator _templateGenerator;

        public MarkdownTemplateEngine(IMarkdownTemplateGenerator templateGenerator)
        {
            _templateGenerator = templateGenerator;
        }

        public string Generate(Data.Models.Template template, object model)
        {
            return _templateGenerator.BuildTemplate(template.Content);
        }

        public async Task<string> GenerateAsync(Data.Models.Template template, object model)
        {
            return await _templateGenerator.BuildTemplateAsync(template.Content);
        }
    }
}
