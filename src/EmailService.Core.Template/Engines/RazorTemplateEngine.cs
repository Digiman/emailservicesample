﻿using System.Threading.Tasks;
using EmailService.Core.Template.Interfaces;

namespace EmailService.Core.Template.Engines
{
    /// <summary>
    /// Engine to create messages based on razor templates.
    /// </summary>
    public sealed class RazorTemplateEngine : ITemplateEngine
    {
        /// <summary>
        /// Generator for Razor templates.
        /// </summary>
        private readonly IRazorTemplateGenerator _templateGenerator;

        public RazorTemplateEngine(IRazorTemplateGenerator templateGenerator)
        {
            _templateGenerator = templateGenerator;
        }

        public string Generate(Data.Models.Template template, object model)
        {

            var result = _templateGenerator.BuildTemplateAsync(template.Name, model, template.Content).Result;

            return result;
        }

        public async Task<string> GenerateAsync(Data.Models.Template template, object model)
        {
            var result = await _templateGenerator.BuildTemplateAsync(template.Name, model, template.Content);

            return result;
        }
    }
}
