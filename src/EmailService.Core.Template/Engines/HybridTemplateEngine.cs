﻿using System.Threading.Tasks;
using EmailService.Core.Template.Interfaces;

namespace EmailService.Core.Template.Engines
{
    /// <summary>
    /// Engine to do transformation from Markdown with data to HTML.
    /// </summary>
    public sealed class HybridTemplateEngine : ITemplateEngine
    {
        private readonly IMarkdownTemplateGenerator _markdownTemplateGenerator;
        private readonly IRazorTemplateGenerator _razorTemplateGenerator;

        public HybridTemplateEngine(IMarkdownTemplateGenerator markdownTemplateGenerator,
            IRazorTemplateGenerator razorTemplateGenerator)
        {
            _markdownTemplateGenerator = markdownTemplateGenerator;
            _razorTemplateGenerator = razorTemplateGenerator;
        }

        public string Generate(Data.Models.Template template, object model)
        {
            return GenerateAsync(template, model).GetAwaiter().GetResult();
        }

        public async Task<string> GenerateAsync(Data.Models.Template template, object model)
        {
            // 1. Render with Razor.
            var renderResult = await _razorTemplateGenerator.BuildTemplateAsync(template.Name, model, template.Content);

            // 2. Render to HTML from Markdown.
            var result = await _markdownTemplateGenerator.BuildTemplateAsync(renderResult);

            return result;
        }
    }
}
