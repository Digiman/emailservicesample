﻿namespace EmailService.Core.Common.Configuration
{
    /// <summary>
    /// Simple configuration for application.
    /// </summary>
    public sealed class ApplicationConfiguration
    {
        /// <summary>
        /// Name of the application.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Simple version string for application.
        /// </summary>
        public string Version { get; set; }

        /// <summary>
        /// Simple description of the application.
        /// </summary>
        public string Description { get; set; }
    }
}
