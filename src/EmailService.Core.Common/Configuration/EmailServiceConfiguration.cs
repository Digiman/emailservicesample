﻿using System.Collections.Generic;

namespace EmailService.Core.Common.Configuration
{
    /// <summary>
    /// Specific Email service configuration.
    /// </summary>
    public sealed class EmailServiceConfiguration
    {
        /// <summary>
        /// List of possible configurations for providers available in the application.
        /// </summary>
        public List<ProviderConfiguration> ProviderConfigurations { get; set; }

        /// <summary>
        /// Default Sender Email.
        /// </summary>
        public string DefaultSenderEmail { get; set; }

        /// <summary>
        /// Default Sender Display name.
        /// </summary>
        public string DefaultSenderDisplayName { get; set; }

        /// <summary>
        /// Is use HTML in mail body?
        /// </summary>
        /// <remarks>By default it need to be set to true.</remarks>
        public bool UseHtml { get; set; } = true;

        /// <summary>
        /// Path to the folder with templates.
        /// </summary>
        public string TemplatesPath { get; set; }

        /// <summary>
        /// File with with list of the all supported templates.
        /// </summary>
        public string TemplatesIndexFile { get; set; }

        /// <summary>
        /// Type of the email provider to use in application to send emails.
        /// </summary>
        public string MailProvider { get; set; }

        public EmailServiceConfiguration()
        {
            ProviderConfigurations = new List<ProviderConfiguration>();
        }
    }

    /// <summary>
    /// Settings for specific provider configuration.
    /// </summary>
    public sealed class ProviderConfiguration
    {
        /// <summary>
        /// Here will be placed name of the provider (Generic, SendGrid and etc.).
        /// </summary>
        public string ProviderName { get; set; }

        /// <summary>
        /// DNS name of the SMTP server to send emails.
        /// </summary>
        public string Domain { get; set; }

        /// <summary>
        /// Port to use to send.
        /// </summary>
        public int Port { get; set; }

        /// <summary>
        /// Username for Email provider.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// API Key or Password for user.
        /// </summary>
        public string Key { get; set; }

        public bool UseSsl { get; set; }
        public bool RequiresAuthentication { get; set; } = true;
    }
}
