﻿using EmailService.Core.Common.Interfaces;
using EmailService.Core.Common.Providers;
using Microsoft.Extensions.DependencyInjection;

namespace EmailService.Core.Common
{
    /// <summary>
    /// Configuration for Common module.
    /// </summary>
    public static class CommonModuleConfig
    {
        /// <summary>
        /// Configure all dependencies for Logic module.
        /// </summary>
        /// <param name="serviceCollection">Services in container.</param>
        private static void Configure(IServiceCollection serviceCollection)
        {
            serviceCollection.AddSingleton<IApplicationConfigurationProvider, ApplicationConfigurationProvider>();

            serviceCollection.AddTransient<IFileManager, FileManager>();
        }

        /// <summary>
        /// Configure DI for Common module components.
        /// </summary>
        /// <param name="serviceCollection">Services in container.</param>
        public static void InitializeCommonModule(this IServiceCollection serviceCollection)
        {
            Configure(serviceCollection);
        }
    }
}
