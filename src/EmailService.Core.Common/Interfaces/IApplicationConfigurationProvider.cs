﻿using EmailService.Core.Common.Configuration;

namespace EmailService.Core.Common.Interfaces
{
    public interface IApplicationConfigurationProvider
    {
        /// <summary>
        /// Get application settings.
        /// </summary>
        /// <returns>Returns object with configuration.</returns>
        ApplicationConfiguration GetApplicationConfiguration();

        /// <summary>
        /// Get settings related to the core of the application - sending emails.
        /// </summary>
        /// <returns>Returns object with configuration.</returns>
        EmailServiceConfiguration GetEmailServiceConfiguration();
    }
}
