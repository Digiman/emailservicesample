﻿using System.IO;

namespace EmailService.Core.Common.Interfaces
{
    /// <summary>
    /// Simple File Manager to work with files on local machine in the application.
    /// </summary>
    public interface IFileManager
    {
        void SaveFileFromStream(string filename, Stream stream);
        void SaveFileFromString(string filename, string content);
        Stream LoadFileToStream(string filename);
        string LoadFileToString(string filename);
        void CreateFolder(string folder);
        bool FileExists(string filename);
    }
}
