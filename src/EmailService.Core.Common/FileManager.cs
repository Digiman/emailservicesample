﻿using System.IO;
using EmailService.Core.Common.Interfaces;

namespace EmailService.Core.Common
{
    /// <summary>
    /// Simple implementation of the File Manager to work with files on local machine in the application.
    /// </summary>
    public sealed class FileManager : IFileManager
    {
        public void SaveFileFromStream(string filename, Stream stream)
        {
            using (var workingStream = new MemoryStream())
            {
                stream.Position = 0;
                stream.CopyTo(workingStream);

                using (var file = new FileStream(filename, FileMode.Create, FileAccess.Write))
                {
                    workingStream.Seek(0, SeekOrigin.Begin);
                    workingStream.CopyTo(file);
                }
            }
        }

        public void SaveFileFromString(string filename, string content)
        {
            File.WriteAllText(filename, content);
        }

        public Stream LoadFileToStream(string filename)
        {
            var ms = new MemoryStream();
            using (var file = new FileStream(filename, FileMode.Open, FileAccess.Read))
            {
                byte[] bytes = new byte[file.Length];
                file.Read(bytes, 0, (int)file.Length);
                ms.Write(bytes, 0, (int)file.Length);
            }

            return ms;
        }

        public string LoadFileToString(string filename)
        {
            return File.ReadAllText(filename);
        }

        public void CreateFolder(string folder)
        {
            Directory.CreateDirectory(folder);
        }

        public bool FileExists(string filename)
        {
            return File.Exists(filename);
        }
    }
}
