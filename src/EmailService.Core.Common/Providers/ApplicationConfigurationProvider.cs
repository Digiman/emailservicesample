﻿using EmailService.Core.Common.Configuration;
using EmailService.Core.Common.Interfaces;
using Microsoft.Extensions.Configuration;

namespace EmailService.Core.Common.Providers
{
    // TODO: move here properties to access to settings more simpler!

    /// <summary>
    /// Provider to work with configuration based on files.
    /// </summary>
    public sealed class ApplicationConfigurationProvider : IApplicationConfigurationProvider
    {
        private readonly IConfiguration _configuration;

        public ApplicationConfigurationProvider(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        /// <summary>
        /// Get application settings.
        /// </summary>
        /// <returns>Returns object with configuration.</returns>
        public ApplicationConfiguration GetApplicationConfiguration()
        {
            return _configuration.GetSection("application").Get<ApplicationConfiguration>();
        }

        /// <summary>
        /// Get settings related to the core of the application - sending emails.
        /// </summary>
        /// <returns>Returns object with configuration.</returns>
        public EmailServiceConfiguration GetEmailServiceConfiguration()
        {
            return _configuration.GetSection("emailService").Get<EmailServiceConfiguration>();
        }
    }
}
