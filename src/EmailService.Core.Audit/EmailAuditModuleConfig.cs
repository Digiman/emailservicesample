﻿using EmailService.Core.Audit.Interfaces;
using Microsoft.Extensions.DependencyInjection;

namespace EmailService.Core.Audit
{
    /// <summary>
    /// Configuration for Email Audit module.
    /// </summary>
    public static class EmailAuditModuleConfig
    {
        /// <summary>
        /// Configure all dependencies for Email module.
        /// </summary>
        /// <param name="serviceCollection">Services in container.</param>
        private static void Configure(IServiceCollection serviceCollection)
        {
            // register providers
            serviceCollection.AddSingleton<IEmailAudit, EmailAudit>();
        }

        /// <summary>
        /// Configure DI for Email module components.
        /// </summary>
        /// <param name="serviceCollection">Services in container.</param>
        public static void InitializeEmailAuditModule(this IServiceCollection serviceCollection)
        {
            Configure(serviceCollection);
        }
    }
}
