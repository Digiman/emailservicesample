﻿using System.Threading.Tasks;
using EmailService.Core.Audit.Interfaces;
using EmailService.Core.Common.Interfaces;
using EmailService.Core.Data.Models;
using Microsoft.Extensions.Logging;

namespace EmailService.Core.Audit
{
    // TODO: implement solution to audit sent emails.

    /// <summary>
    /// Solution to track whats happened with emails - audit email sending.
    /// </summary>
    public sealed class EmailAudit : IEmailAudit
    {
        private readonly ILogger<EmailAudit> _logger;
        private readonly IApplicationConfigurationProvider _applicationConfigurationProvider;

        public EmailAudit(ILogger<EmailAudit> logger, IApplicationConfigurationProvider applicationConfigurationProvider)
        {
            _logger = logger;
            _applicationConfigurationProvider = applicationConfigurationProvider;
        }

        public void SaveEmailAudit(EmailAuditModel model)
        {
            // TODO: implement the logic to save email audit
        }

        public async Task SaveEmailAuditAsync(EmailAuditModel model)
        {
            // TODO: implement the logic to save email audit
        }
    }
}
