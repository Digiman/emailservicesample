﻿using System.Threading.Tasks;
using EmailService.Core.Data.Models;

namespace EmailService.Core.Audit.Interfaces
{
    public interface IEmailAudit
    {
        void SaveEmailAudit(EmailAuditModel model);
        Task SaveEmailAuditAsync(EmailAuditModel model);
    }
}
