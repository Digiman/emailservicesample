﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using EmailService.Core.Data.DTO.V1;
using EmailService.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace EmailService.API.Controllers.V1
{
    // TODO: implement here controller with logic to work with templates inside application through API
    // add actions like get list of templates and get template by name (first)
    // maybe later - add possibility to add new template with API (add a ew template, POST)

    /// <summary>
    /// Template controller to work with templates in EmailService.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public sealed class TemplateController : ControllerBase
    {
        /// <summary>
        /// Mapper to convert objects.
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// Service to handle all logic about templates inside emails service elated to use for consumers.
        /// </summary>
        private readonly ITemplateService _templateService;

        public TemplateController(IMapper mapper, ITemplateService templateService)
        {
            _mapper = mapper;
            _templateService = templateService;
        }

        /// <summary>
        /// Get all available templates in the application.
        /// </summary>
        /// <returns>Returns list of template models.</returns>
        [HttpGet]
        public async Task<IActionResult> GetAllTemplatesAsync()
        {
            try
            {
                var templates = await _templateService.GetAllTemplatesAsync();

                var result = _mapper.Map<List<TemplateModelDTO>>(templates);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get template data by name.
        /// </summary>
        /// <param name="name">Name of the template - unique.</param>
        /// <returns>Returns template data.</returns>
        [HttpGet("{name}")]
        public async Task<IActionResult> GetTemplateByNameAsync(string name)
        {
            try
            {
                var template = await _templateService.GetTemplateAsync(name);

                var result = _mapper.Map<TemplateDTO>(template);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}