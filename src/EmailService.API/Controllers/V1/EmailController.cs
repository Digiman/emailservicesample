﻿using System;
using System.Threading.Tasks;
using AutoMapper;
using EmailService.Core.Data.DTO.V1;
using EmailService.Core.Data.Models;
using EmailService.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace EmailService.API.Controllers.V1
{
    /// <summary>
    /// Controller to work with emails that can be send with service.
    /// </summary>
    [ApiVersion("1.0")]
    [Route("api/v{version:apiVersion}/[controller]")]
    [ApiController]
    public sealed class EmailController : ControllerBase
    {
        /// <summary>
        /// Main service to handle all logic related to sending emails.
        /// </summary>
        private readonly IEmailService _emailService;

        /// <summary>
        /// Specific logic to proceed data fro email to place into templates.
        /// </summary>
        private readonly IEmailDataSerializer _emailDataSerializer;

        /// <summary>
        /// Mapper to work with DTO objects.
        /// </summary>
        private readonly IMapper _mapper;

        public EmailController(IEmailService emailService, IMapper mapper, IEmailDataSerializer emailDataSerializer)
        {
            _emailService = emailService;
            _mapper = mapper;
            _emailDataSerializer = emailDataSerializer;
        }

        /// <summary>
        /// Send simple email.
        /// </summary>
        /// <param name="model">Model with information to send.</param>
        /// <returns>Returns result about sending email.</returns>
        [HttpPost]
        [Route("SendEmailSimple")]
        public async Task<IActionResult> SendEmailAsync([FromBody] EmailModelDTO model)
        {
            try
            {
                var emailModel = _mapper.Map<EmailModel>(model);

                // send email
                var emailResult = await _emailService.SendEmailAsync(emailModel);

                // convert internal result model to DTO
                var result = _mapper.Map<EmailResultDTO>(emailResult);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Send email with template that allow to insert data to needed places.
        /// </summary>
        /// <param name="model">Model with full details to send email that uses templates.</param>
        [HttpPost]
        [Route("SendEmail")]
        public async Task<IActionResult> SendEmailWithTemplateAsync([FromBody] EmailModelTemplateDTO model)
        {
            try
            {
                var emailModel = _mapper.Map<EmailModel>(model.Model);

                // process data for templates from initial object
                var data = _emailDataSerializer.Deserialize(model.Data);

                // send email
                var emailResult = await _emailService.SendEmailAsync(emailModel, model.TemplateName, data);

                // convert internal result model to DTO
                var result = _mapper.Map<EmailResultDTO>(emailResult);

                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}