﻿using System;
using System.IO;
using AutoMapper;
using EmailService.Core.Audit;
using EmailService.Core.Common;
using EmailService.Core.Email;
using EmailService.Core.Template;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace EmailService.API
{
    // TODO: add Serilog + Sink for splunk
    // TODO: add CORS support
    // TODO: configure logger and related services
    // TODO: Add Api Key to add functionality to has authorized apps
    // TODO: Add Versioning to API.

    // to configure Swagger see here: https://docs.microsoft.com/en-us/aspnet/core/tutorials/getting-started-with-swashbuckle?view=aspnetcore-2.2&tabs=visual-studio

    /// <summary>
    /// Main class with configuration for all middleware components in API.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Configuration for application loaded from the files.
        /// </summary>
        private IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            // initialize logger with configuration from file
            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(configuration)
                .CreateLogger();
            
            Configuration = configuration;
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">Services collection to configure.</param>
        public void ConfigureServices(IServiceCollection services)
        {
            // NOTE: later make sure that capability version is properly set! (for now it's 2.2+)
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

            // add API Version support
            services.AddApiVersioning(options =>
            {
                // reporting api versions will return the headers "api-supported-versions" and "api-deprecated-versions"
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
            });

            // add API Explorer - to work with API versions
            services.AddVersionedApiExplorer(
                options =>
                {
                    // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service
                    // note: the specified format code will format the version as "'v'major[.minor][-status]"
                    options.GroupNameFormat = "'v'VVV";

                    // note: this option is only necessary when versioning by url segment. the SubstitutionFormat
                    // can also be used to control the format of the API version in route templates
                    options.SubstituteApiVersionInUrl = true;
                });

            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, ConfigureSwaggerOptions>();


            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(options =>
            { 
                // add a custom operation filter which sets default values
                options.OperationFilter<SwaggerDefaultValues>();

                // Set the comments path for the Swagger JSON and UI.
                var xmlDocFiles = new []
                {
                    "EmailService.API.xml",
                    "EmailService.Core.Common.xml",
                    "EmailService.Core.Data.xml"
                };
                foreach (var xmlDocFile in xmlDocFiles)
                {
                    var xmlPath = Path.Combine(AppContext.BaseDirectory, "Docs", xmlDocFile);
                    options.IncludeXmlComments(xmlPath);
                }
            });

            // add Auto Mapper
            services.AddAutoMapper();

            // add all DI modules
            services.InitializeCommonModule();
            services.InitializeTemplateModule();
            services.InitializeEmailModule();
            services.InitializeEmailServiceModule();
            services.InitializeEmailAuditModule();

            // configure CORS
            //services.AddCors();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">Application builder.</param>
        /// <param name="env">Environment settings.</param>
        /// <param name="provider">API Explorer version description provider.</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, IApiVersionDescriptionProvider provider)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");

                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            // Enable middleware to serve generated Swagger as a JSON endpoint.
            app.UseSwagger();

            // Enable middleware to serve swagger-ui (HTML, JS, CSS, etc.), 
            // specifying the Swagger JSON endpoint.
            app.UseSwaggerUI(options =>
            {
                // build a swagger endpoint for each discovered API version
                foreach (var description in provider.ApiVersionDescriptions)
                {
                    options.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json", description.GroupName.ToUpperInvariant());
                }
            });

            // configure CORS
            //app.UseCors();

            // add MVC and API middleware
            app.UseMvc();
        }
    }
}
