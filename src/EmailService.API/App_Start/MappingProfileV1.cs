﻿using AutoMapper;
using EmailService.Core.Data.DTO.V1;
using EmailService.Core.Data.Models;

namespace EmailService.API
{
    /// <summary>
    /// Default profile for all mapper settings for API.
    /// </summary>
    public sealed class MappingProfileV1 : Profile
    {
        public MappingProfileV1()
        {
            CreateMap<EmailModelDTO, EmailModel>();
            CreateMap<EmailModel, EmailModelDTO>();

            CreateMap<EmailAttachmentDTO, EmailAttachment>();
            CreateMap<EmailAttachment, EmailAttachmentDTO>();
            
            CreateMap<EmailResultDTO, EmailResult>();
            CreateMap<EmailResult, EmailResultDTO>();

            CreateMap<ErrorDetailDTO, ErrorDetail>();
            CreateMap<ErrorDetail, ErrorDetailDTO>();

            CreateMap<TemplateDTO, Template>();
            CreateMap<Template, TemplateDTO>();

            CreateMap<TemplateModelDTO, TemplateModel>();
            CreateMap<TemplateModel, TemplateModelDTO>();
        }
    }
}
