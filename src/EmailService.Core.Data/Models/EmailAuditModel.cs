﻿namespace EmailService.Core.Data.Models
{
    // TODO: implement the model to use in audit of email sending

    /// <summary>
    /// Model to store information about email sending.
    /// </summary>
    public sealed class EmailAuditModel
    {
        public string TemplateName { get; set; }
        public string Message { get; set; }
    }
}
