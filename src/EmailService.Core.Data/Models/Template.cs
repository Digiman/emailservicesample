﻿namespace EmailService.Core.Data.Models
{
    /// <summary>
    /// Full template data that required to process within the application.
    /// </summary>
    public sealed class Template
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Content { get; set; }
    }
}
