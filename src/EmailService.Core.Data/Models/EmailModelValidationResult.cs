﻿using System.Collections.Generic;
using EmailService.Core.Data.Enums;

namespace EmailService.Core.Data.Models
{
    /// <summary>
    /// Validation result after processing Email Model with validator.
    /// </summary>
    public sealed class EmailModelValidationResult : ValidationResultBase
    {
    }

    public class ValidationResultBase
    {
        /// <summary>
        /// Status od the validation.
        /// </summary>
        public ValidationStatus Status { get; set; }

        /// <summary>
        /// List of errors.
        /// </summary>
        public List<ErrorDetail> Errors { get; set; }

        public ValidationResultBase()
        {
            Errors = new List<ErrorDetail>();
        }
    }
}
