﻿using System.Collections.Generic;

namespace EmailService.Core.Data.Models
{
    // TODO: add here more properties to use to send emails! all possible data that can be used to create an email!

    /// <summary>
    /// Model with all data for email to send.
    /// </summary>
    public sealed class EmailModel
    {
        /// <summary>
        /// Email from - sender.
        /// </summary>
        public string FromEmail { get; set; }

        /// <summary>
        /// Name of the sender from.
        /// </summary>
        public string FromDisplayName { get; set; }

        /// <summary>
        /// List of emails to send to.
        /// </summary>
        public string To { get; set; }

        /// <summary>
        /// List of emails to copy (CC).
        /// </summary>
        public string Cc { get; set; }

        /// <summary>
        /// List of emails to copy (BCC).
        /// </summary>
        public string Bcc { get; set; }

        /// <summary>
        /// List of email to reply.
        /// </summary>
        public string Reply { get; set; }

        /// <summary>
        /// Mail subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Mail body.
        /// </summary>
        /// <remarks>Usually as HTML.</remarks>
        public string Body { get; set; }

        /// <summary>
        /// List of email attachments. 
        /// </summary>
        public List<EmailAttachment> Attachments { get; set; }

        public EmailModel()
        {
            Attachments = new List<EmailAttachment>();
        }
    }

    /// <summary>
    /// Attachment to email with data to send with email.
    /// </summary>
    public sealed class EmailAttachment
    {
        // TODO: create some logic to work with email attachments!

        /// <summary>
        /// Filename for file in attachment.
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Base64 encoded string.
        /// </summary>
        public string FileData { get; set; }

        /// <summary>
        /// MIME type for file.
        /// </summary>
        public string ContentType { get; set; }
    }
}
