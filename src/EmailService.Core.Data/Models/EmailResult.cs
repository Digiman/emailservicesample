﻿using System.Collections.Generic;
using EmailService.Core.Data.Enums;

namespace EmailService.Core.Data.Models
{
    // TODO: think here to add some more details to result

    /// <summary>
    /// Model with information about processing the email.
    /// </summary>
    public sealed class EmailResult
    {
        /// <summary>
        /// Status of the processing.
        /// </summary>
        public ResultStatus Status { get; set; }

        /// <summary>
        /// Error message.
        /// </summary>
        public string ErrorMessage { get; set; }

        /// <summary>
        /// List of errors happened during processing.
        /// </summary>
        public List<ErrorDetail> ErrorDetails { get; set; }

        public EmailResult()
        {
            ErrorDetails = new List<ErrorDetail>();
        }
    }

    /// <summary>
    /// Error details.
    /// </summary>
    public sealed class ErrorDetail
    {
        /// <summary>
        /// Error type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Error message with details about error.
        /// </summary>
        public string Message { get; set; }
    }
}
