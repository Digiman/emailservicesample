﻿namespace EmailService.Core.Data.DTO.V1
{
    /// <summary>
    /// Model with information about template and content.
    /// </summary>
    public sealed class TemplateDTO
    {
        /// <summary>
        /// Name of the template. Unique value.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Some description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Type of the template.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Content for the template.
        /// </summary>
        public string Content { get; set; }
    }
}
