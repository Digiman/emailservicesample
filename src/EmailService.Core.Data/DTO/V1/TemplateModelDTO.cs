﻿namespace EmailService.Core.Data.DTO.V1
{
    /// <summary>
    /// Model with metadata for Template.
    /// </summary>
    public sealed class TemplateModelDTO
    {
        /// <summary>
        /// Name of the template. Unique value.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Some description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Type of the template.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Content of the template - instead of the file.
        /// </summary>
        public string Content { get; set; }

        /// <summary>
        /// Is template placed in the file?
        /// </summary>
        public bool IsTemplateInFile { get; set; }

        /// <summary>
        /// Filename with template.
        /// </summary>
        public string TemplateFile { get; set; }
    }
}
