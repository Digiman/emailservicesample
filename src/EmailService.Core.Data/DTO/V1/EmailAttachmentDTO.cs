﻿namespace EmailService.Core.Data.DTO.V1
{
    /// <summary>
    /// DTO to represent file as attachment to email.
    /// </summary>
    public sealed class EmailAttachmentDTO
    {
        /// <summary>
        /// Filename for file in attachment.
        /// </summary>
        public string Filename { get; set; }

        /// <summary>
        /// Base64 encoded string.
        /// </summary>
        public string FileData { get; set; }

        /// <summary>
        /// MIME type for file.
        /// </summary>
        public string ContentType { get; set; }
    }
}