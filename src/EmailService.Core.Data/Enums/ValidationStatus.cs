﻿namespace EmailService.Core.Data.Enums
{
    /// <summary>
    /// Validation statuses.
    /// </summary>
    public enum ValidationStatus
    {
        None = 0,
        Valid = 1,
        NotValid = 2
    }
}
