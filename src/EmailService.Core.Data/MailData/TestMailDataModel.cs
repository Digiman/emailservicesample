﻿using System;

namespace EmailService.Core.Data.MailData
{
    /// <summary>
    /// Simple test model to send email for testing purpose.
    /// </summary>
    public sealed class TestMailDataModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Year => DateTime.Today.Year;
    }
}
