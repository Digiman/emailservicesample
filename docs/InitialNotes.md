# Some notes on shared Email service or functionality

Main idea of the email service came from complexity of the system in general and using a lot of application (old, legacy, new and in future in development) and different kind of them. If something changed in configuration it required to update all the places in this app to make sure that it will work properly.
It tales much time and also need to know all the places where it required to do (source code and rebuild the application, configuration files and etc.).

This work started in scope of looking for all places where we use old SMTP server to migrate to new one. After investigation we found that almost all place in C# code and it's in EXE Jobs and in some of the web applications (IRWeb). In some places it hard-coded in some - place in XML configuration file.

## Proposed solutions

We started working with some ideas and possible solutions to send emails.
Main idea is to provide some basic functionality to send emails and also a new functionality like using templates to generate emails based on them and insert data. This solution need to be extendable and can grow in future - this goal need to take carefully designed and developed application to support in future.

Based on this we can have at least 2 solutions.

### Solution 1

Basic solution it can be creation a new NuGet package that will include all needed logic to send emails and etc. in one place. This will be a new project (solution) and new NuGet package with all related staff (build, push to nuget feed, publish and etc.).

![Image 1 - Solution 1](\images\NewSampleEmailService-Feb2019-Idea-Solution1.png)

This solution is most simple but problem here in configuration. Every time when you need to improve something, create a new logic, add templates and etc. you need to create a new version of the package and update for all application that used this package. It takes much time, It's not a big problem but required a lot of actions.

Also if we need to update configuration settings it need to update file inside original solution and later update configuration for all of the application with this package.

Main question here is how to better deal with configuration for SMTP servers or any server that do actual email sending job. 

Cons:

* Simple solution - all in one place in one NuGet package.
* Basic flow to update packages.

Pros:

* Need much time to update configuration and packages for actual applications and redeploy them.

### Solution 2

This solution partially the same as solution 1 but it extended with special shared service that build independently and can be changed without reflection to the actual application - this main feature of this solution - all changes can be made in independent service with extended functionality and applications will only consume API.

![Image 2 - Solution 2](\images\NewSampleEmailService-Feb2019-Idea-Solution2.png)

Idea here is to provide independent service and consumer libraries for it to use from any kind of application. Here also it can be created to use in JavaScript/TypeScript from Front end application not only in C# applications (because it required to use public API that can called from any platform and any language).

This service will be manageable and extendable. It very simply to add a new template for new email type or kind and not needed to update all applications - only EmailService.

Cons:

* Independent service that can be developed and extended without problems and reflections to the actual application.
* Single place for configuration settings and extension point. Only need to update service and deployed a newest version and it's all. It not required to update packages and configuration for all consumer applications.
* Logic can be extended in very simple way.
* Template generation functionality inside.
* It possible to add or update email template very simple in service without changes in consumer applications.
* Update NuGet packages for consumer application required only when breaking changes (in general, ideally).
* This service can be developed and manages by any of the developers from external team (like Exadel team). It needed at least one developer and one time setup for DevOps pipeline.

Pros:

* Need to manage all the new service and deploy and host independently. (maybe it cons from real vision)

