﻿# EmailService API - new way to send emails

This is very briefly notes and information about using EmailService API.

Here will be provided some details about service and samples how to use it. Later also it will be created full documentation for this.

## Introduction

EmailService is an independent application. It means that need to have same way to access to this service from other applications (even from different kind of them).

## Send emails

API supported to send emails in 2 ways:
1. Send simple email - body predefined and already provided.
2. Send emails with templates - body will be generated from specific template.

API endpoints to send emails:
1. Email: 
    1.1. POST: */api/Email/SendEmailSimple*
    1.2. POST: */api/Email/SendEmail*
2. Template:
    2.1. GET: */api/Template*
    2.2. GET: */api/Template/{name}*

Application supported to send emails in two ways:
1. Simple simple email - with already prepared body.
2. Email with template with custom data.

### Models to send the emails

Here provided some models that used to send emails in EmailService API.

Basic email model:
```json
{
  "fromEmail": "string",
  "fromDisplayName": "string",
  "to": "string",
  "cc": "string",
  "bcc": "string",
  "reply": "string",
  "subject": "string",
  "body": "string"
}
```

This model working fine if you don't need to add attachments (files) to send with email. Next model include list of files to be added to email. Same endpoint.

Email model with attachments:
```json
{
  "fromEmail": "string",
  "fromDisplayName": "string",
  "to": "string",
  "cc": "string",
  "bcc": "string",
  "reply": "string",
  "subject": "string",
  "body": "string",
  "attachments": [
    {
     "filename": "string", // original file name (name and extension but not full path!)
      "fileData": "string", // file content ion Base64 string!
      "contentType": "string" // MIME type for file!
    }
  ]
}
```

Model that support also template - here needed to add template name (make sure that it's available in EmailService) and data for template (JSON in string).

Email with template:
```json
{
  "model": { // Main model with data to send email
    "fromEmail": "string",
    "fromDisplayName": "string",
    "to": "string",
    "cc": "string",
    "bcc": "string",
    "reply": "string",
    "subject": "string",
    "body": "string", // here body is not required because it will be generated from template!
    "attachments": [
      {
        "filename": "string", // original file name (name and extension but not full path!)
        "fileData": "string", // file content ion Base64 string!
        "contentType": "string" // MIME type for file!
      }
    ]
  },
  "templateName": "string", // Template name (unique)
  "data": "string" // object in JSON format 
}
```

---

## Samples

In this section provided some samples that works and provide some basic staff to test API and see how it works. This samples are very simple and idea here is to provide basic functionality to test and show actual results.

NOTE: make sure that you are provided correct email addresses! For initial samples used addresses of developer who start working with service.

### 1 Send simple email

This sample need to send simple email with already prepared body without using template generation logic.
```json
{
    "fromEmail": "digiman89@gmail.com",
    "to": "akukharenko@exadel.com",
    "subject": "Test mail from with API",
    "body": "Hello, EmailService API email sender!"
}
```

### 2 Send emails with template

Sample with test model and template for Razor:
```json
{
    "model": {
        "fromEmail": "digiman89@gmail.com",
        "to": "akukharenko@exadel.com",
        "subject": "Test mail from with API",
        "body": "Hello, EmailService API email sender!"
    },
    "templateName": "TestRazor",
    "data": "{\"Name\":\"test name\",\"Description\":\"test description\"}"
}
```

Sample with same model but with attached small image:
```json
{
    "model": {
        "fromEmail": "digiman89@gmail.com",
        "to": "akukharenko@exadel.com",
        "subject": "Test mail from with API",
        "body": "Hello, EmailService API email sender!",
        "attachments": [
            {
              "filename": "E-470 Logo.png",
              "fileData": "iVBORw0KGgoAAAANSUhEUgAAAUQAAAD2CAYAAACun/4MAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyJpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMy1jMDExIDY2LjE0NTY2MSwgMjAxMi8wMi8wNi0xNDo1NjoyNyAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNiAoV2luZG93cykiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6MUE0NkI5MEJBRDQ1MTFFNEI3N0RBRkUyODc5NzY1QTMiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6MUE0NkI5MENBRDQ1MTFFNEI3N0RBRkUyODc5NzY1QTMiPiA8eG1wTU06RGVyaXZlZEZyb20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDoxQTQ2QjkwOUFENDUxMUU0Qjc3REFGRTI4Nzk3NjVBMyIgc3RSZWY6ZG9jdW1lbnRJRD0ieG1wLmRpZDoxQTQ2QjkwQUFENDUxMUU0Qjc3REFGRTI4Nzk3NjVBMyIvPiA8L3JkZjpEZXNjcmlwdGlvbj4gPC9yZGY6UkRGPiA8L3g6eG1wbWV0YT4gPD94cGFja2V0IGVuZD0iciI/PjP/9G4AABM7SURBVHja7J09bxvJGYBXPAMHJDhENtIGoZs4aWKqijtTP+BgCkhv8RdY6lKdravSSfoFotsggHjID9C6cyqtqsApYgKpAgQOjQQBDjiAmbGHtqSI4i75vvO1zwMQ8vnuqN35eOZ9Z2ZnO7PZrFjnA+CLP377p03zGVASsAp1fNahmCAVGZofZ+Zzav68S4mABggRUpJhz/3VCVIEhAjI8DNIERAiIEOkCAgRkCFSBIQIyHApSBEQIiBDpAgIEZAhUgSECMgQKQJCBGSIFAEhAjJEioAQARkiRUCIgAyRIiBEQIZIERAiIEOkCAgRkCFSBIQIyDAYSBEQIiBDpAgIEZAhUgSECMgQKQJCBGSIFAEhAjJEioAQARkiRUCIgAyRIiBEiE+GvcRliBQBIQIyRIqAEEFahpuZ3RpSRIgAyBApIkQAZIgUESJFAMgQKQJCBGSIFAEhAjJEioAQARkiRUCIgAyRIiBEQIZIERAiIEOkCAgRkCFSBIQIyBApAkIEZIgUASECMkSKgBABGSJFQIiADJEiIERAhkgRECIgQ6QICBGQIVIEhAjIECkCQgRkiBQBIQIyRIqAEAEZIkVAiIAMkSLFgBABGQJSRIiADAEpIkRAhrJMkCIgRECGRTE0ny3zqZAiIESoy/McZfjbb74emc/U/HkbKQJChCaRVE7C+CDD+T8gRUCIUJvMhHFFhkgRECK0VYo3yhApAkKEtknxVhkiRUCI0BYp1pIhUgSECLlLsZEMkSIgRMhViivJECkCQoTchLGWDJEiIETIRRgiMkSKgBAhdWGIyhApAkLMnN/97JtN8xlkKAwVGd5wj1OkCAgxExkWHw9qODV/Fu0EgaWoKkOkCAgxXxn25p0gEyl6keGle6yQIiDEvGRYZCJFrzJEioAQ85Vh6lIMIkOkCAgxXxmmKsWgMkSKgBDzlWFqUoxChkgREGK+MkxFilHJECkCQsxXhrFLMUoZIkVAiPnKMFYpRi1DpAgIMV8ZxibFJGSIFAEh5ivDWKSYlAyRIiDEfGUYWopJyhApAkLMV4ahpJi0DJEiIMR8ZehbilnIECkCQsxXhr6kmJUMkWK72ZjNZut9wcYGpRivDK+ktL//+7cjaqMZRh49V3ebmd1algPZbdRxHRFiO2SoEim2ASJFUmbIT4ZIESkiRYSIDJEiUkSKCBEZIkWkiBQRIjJEikgRKSJEZIgUkSJSRIjIECkiRaSIEJEhUkSKSBEhIkOkiBSRIkJEhkgRKSJFhIgMkSJSRIoIERkiRaSIFBEiMkSKSBEpIkRkiBSRIlJEiMgQKSJFpIgQkSFSRIpIESEiQ6SIFJEiQkSGSBEpIkWEiAyRIlJEiggRGSJFpIgUESIyRIqAFBEiMkSKgBQRYrIy7CJDpIgU2y1FhPhRhlaC58gQKSLFdkux9UJ0MszxReRIESkiRYSIDJEiUkSKCBEZIkWkiBQRIjJEikgRKSJEZIgUkSJSRIjIECkiRaSIEJEhUkSKSBEhIkOkiBSRIkJEhkgRKSJFhIgMkSIgRYSIDJEiIEWEiAyRIiBFhIgMkSIgxQikmJUQkSFSRIpIESEiQ6SIFJEiQkSGSBEpIkWEiAyRIiBFhIgMkSIgRYSIDJEiIEWEiAyRIiBFP1JMTojIECkCUkSIyBApAlJUlmIyQkSGSBGQorYUkxAiMkSKSBEp+pBi9EJEhoAUkaIvKW7MZrP1vmBjQ+2O3z160P3zv391/kPxBTKED/z6R387+uqL/76nJBr2pZ/+4uH7u91Bbvf14//8Y+uXp3+o6vy3dVx3J/L77f3mq78gQ7jMHkXQnHv//OuHD6SdMveoIgBYKPrXbyrJ74tdiI+pcgBYQNm2CLFPnQPAAirpL4xWiO8ePSBdBoDbeNWmCBEhAgARooP5QwBYxPTe6zcTIkQAAIUFFYQIAKly0Rohvnv0oE99AwAR4kcQIgDcRqXxpbEK8SH1DQCLZHjv9RuVwypiFSLzhwDgNTqMUojvHj2whzl0qXMAWMCF1hfHGCH2qW8ACBEhxnj8l40QS+ocAG7i3us3an6I+oBYAAAp6riuQzEBACBEAACECACAEAEAECIAAEIEAECIAAAIEQAAIQIAIEQAAIQIAIAQAQAUuEMRAMBK8vjyS3syVZ3DnKsfvv9+msQ9Ua0AUEN+fSc/+770btHwVHvz/38Qo/vYA17HRpKT2O4ziuO/TGGd0eSuMIyxsdwSJZwUH8+xlGTflEG15HfbTnlIG1Gr14H5PCk+Htq8qfBr7PWPzefYR3uv47rgQjQFb0ebtzjwE1PTOO4mJMOzQucdOHeXpVnm978wP57TRsQjwWdOhj4pzefA3FcZUogxLKrwQqmrVClcpLIMJzXnnB7TRsTqc9d83ro6HQS4JyviM3MNpy5ICkIMQmxro17Eq5bLsEmH79FG1q7LgROhnfboRnBvVsbnVtBtFSIRYkIRogcZ1urwLorYpI2sXI9dN3d/WsT3lssP89Lm+k7aKMQ+DrxC2XIZ1u3wPdrIyvX4ovg4bx9737Np/Jlrd/kL0a0Swmcmse7X8ijDoubEeo820rwOXVSY0kJU36XzrYgQiQ4TSJd9yrBBGbCg0jz4eJtonxv4Sp9DC/EhDrzCRctl2KTDs6BSvw53zY/zIu05V5s+q69+hxYiKfNVypguJoAMa3V4FlQay/Akk3s/0Z5PRIgRobkpNREZ1u3wfdpI62RYuEFQ9cmkTsAO19pGLTHyZyrDYtnjeo62TrW0WYaXU+dujhEiQoxQiCFl2KDD92gjrZThHLVV8pBCZEHlKsGfUAkswyZl0NbB9KJGHWofeGGlvG8+2yaa37j+sX/v/r3mAK8WJYYUIvOHEUWIEciwVhm0fO9qWbMONRYeRuZz30hvy3yOFs1l2r93/37LyXGiVBYqK85BTrtROuHGVtjLVFt6yAWVSGRYuA43qXGtsUtRI0pbesKN23QtHT3bQWpYc27XZ9uqnHRrU8d1dwI2GPF0K6ZV2lSISIaTOmfiuac0yojLc7eQn+Oy9ztc8ntfKMhwZMp7uOZAPzXXtq3Qxno2sJI+RzFUyqzR+ZI4NgsZ5lt/pjz3CvnDcm36uX1bx3fTCNISHq4rw2uD2FChyKUHgGBCFH/satWQHhmuJUPJaO0i8fI8EU6Tp05K+zX+W+kVZXta+Uihf46Er1PcI7lEiKTKzTkVkOE2dfhJhrvCMtyuIyUXlUr2J5smHykV1UHsmWYnQOPpFfKrYK8KaNqB+wIdthJMW6qEy1JShpNLZVsnypdMlSupNHlBlDgRruf0hVgwf5h6B/4kQ8GnjZJ5TaWyDG073mow/XMoGFzY8t/xUGwvheugm7oQNTZkI0TPMhQe3KoWluVNZbBdd2BwIpD8/Qee3vQoXdfJC7Ev/H2TVF7ZmZkMJQe3i5aV5VoydEinykc+yi72rXE5pMxEh2FkKDm4lS0qy7VlqBAd7ifcvEV90vHcmPoKX5v0do1UZegm9EXSlVS2TCmtJu+sMH8qGR2WAaI2yd8nukDrO0LsRV64yLBeZChZl2UiZblb6GytmTS8jk3h6zigp4QTosaGbIToX4aS6XKVQFnagwSkNz/vrBgZ70mm6/SfvCJE5g/DyFBycLuIvCx7CjIcriGip4LXcUxvCSREyTknhKgqw7rRS/Yps2uz9oke6WeTR2tEqlJ9yA56Y3pMuAhRY/6QBRV5GS6NXgRf8jSNfMvUmfAgXtZ8NnkRTwSvZZziZnjtoMjn8V99he88NJ3zMJWacycKxy7DOtFL9huyXXlKDuJW/DtrXI8dgCQPRf0uYPFKukBU6j4jxLa+WFy983uWoWRdRvkMusKK8nwaYp3OOxBM3W1kTrqcYcrceiEGkKFkXZYRylDjtOt9gb2WkgHFOGD5dmPuVx2PhdDWF4urRUOBZCiZ8kSVMru0VPqA17HQY3G5pMuiQpSeB/UVIfYLqIQ7bxAZCr7kaRLhpP7zQn7ecChQ131hSYeMzHux9imfQmz9K0clH08LGBlKDm5RpctuS8ue8NcOhaQvGVCUgQeinwsPOEkKse3zh6Vgxw0pQ8nBLZotU5dSZUkOBJ8CkZw/DL2QJekC8TZEypxQuhyBDCUbdEzzh9LzhvaRuBeR9p/QkXm7U2alE25S45VAOcYgQ7EGHcsztC5Vln7p+TDi/lMFLOuu9MCTYoTY9nR57YqLRYaSrwzIPFWO9b0hoV/VIHkvKk85+RBi2xdU1qq4iCJDydStjKRuYk+VpftP6IFIci5UpQ0RIeqzcsVFJkPJzhl8QcVFu9Kp8n7k/Sd0uUum/yqLQx3lRreJEFdrhBHKULJzBo1UlFLlI6V50agXIQK6IMkIkfnDFSouRhm6CfGu0BRC6NRtr5B9YsLOy4mfPB37Y24Bo0O1NtRJqBBSpWrYCWKMDHOKDq1kngt/7b7SYoWoEAMvqEgeXVaqtQ/lQhBfUNE8Qis0EctQUoihNwZLp8qlUnlLZ1hl4HKXDI7UnsVOLUIMXaltlaFFaoUw5DxWX6FNar7CczOTtt0TjnbVTuvpKBZCV6FCs3xlQAIylBzcQg5q0tHhSHk+9CeZ9B3J98ConvStmTLzygB/MrQN5Kn5rqcJ3PKpuc5G92Y6wI5AOWsspGi/4F2yD70PWOfJHF2mKUSNE7KzihDdY2O7Al+1WaSzgNX0OkuBcrblI72QcpzJO0l8TFNIDUTqL8bSnEOUjhBj2K6RQhSdGxKLMHuF7PTNxHyOqJq80mVtIUpHLDnOHz6mv+hGiC46fCZ8TQdEh7XKvlvIvpvmpfY1d5QKQiPyeZVhm+nTbdQHwufS0aHy4lVOSMqw8nFCklaE2IuwY8Q2epIu15PPdI0y7hbyp2AfUC1BIvNjH9etJUSNVLDMrM0gRD/RoWgbJDqsjeS8rbeoPJUIcZLhnM1D+sxSVp4mUZi/Sj069DZf7cr+WYrlnooQc1xQ6eM71XqXXkgpA5zyLTlv3vV43YcpRocqQlR6ZUCOCyqkzEtYVUBu/ko6OjxOvDi7rly0o0PpVzJ4jco1IkQNIea2oEJ0qFvn4vsOjZzHiZXBTQyU27X0OZPe52w1hKhxwk2ZWWcnOqzRGdbolOL7DgOVwUT4+54oX++Z8EDkvdw7CXR2NmS3k1WfWx8Umew7VHgya6Bw6Ox8IDoR7vtHIQKhjnCh2MKWLvAchUiEqFfv4s8spxgp38Khkgx3hSPjIFF5J4GOntWCikvpugXcxkrPrQsfJPDhOsxnFLgspNu/jRJ3pdqy+ZwW8gtYw1Db7KRPu9EQ4okbgVLkYMFrKbcTuHY73yTxlEe5wmi/ameQnjscRbD/dawQ9do+VawzFeAGn0OFPn8Ucs1AWojMjS1J+1wHK2O/cNPgpSbgX/lo4G66RnoVNfhWGxspm3ubKGQVJ67MjppI/9I7aXY1+ou5lv2Q5Z1CypyVEBNCqi59yV86OrRHTU0iqQutU16s2N4ayb247dl6K0GbZrv0+K2SDKcxZE4bs9lsvS/Y2JgXmi3Qcxz4uYJNh7qb6sWb+pwJfdVdH2mnud5/FbKryzuB9h4uisreevp11wewvo++YmWofd5pHddJpsxEh2EiI40OKNUJKk8y1NhqM46lPmykau5xpBSZFQEEeJ3tWA5/lkyZOazgKim//yW1dzBLv0vmZYR1clCsvtgUM8NlMrTZpx2kfTzhJSnEfgFZRIiCg5v6oOC2MUkvpoxiqxA3n3mcUf+YFkveFOlEaKcK7FScfQrmzE6NXN42ZP5s/27mPt0bvuN8/u99C5GUOUx0FHPa5KMMpNPImBZTrkvxRZHHgwrzOcPRkoFu/iigfevitvtpOVlwUMXute/oNvWSiBA5rOD/SPb8RsmN4572k0mny99FXkU7iafOVVFvzrDnZGjfbmgHqdLN6+677Kt7LRub3tAWnjUdmKUiRKLDfNLlZLbbrBIB1IkQY64cF71uJ9q2xkX9BZS59J+4RbP5/dvN8jd9h/3u7rXtQwP397UHECkhsqBylZQXVFJKl6XnDscpRPZOBsPEUuR9c907dcvX3ePIDXinbh7Q/txbkC4fX84YnBi7TSP+TmSdKBdSnudJZkGlhenyZWFYWWwlkD7bTGHLXO/RCvc4dPe47yI9OwDaxwXPr0vRCXRyaZC0bWPa9PHEtYXIYQU3VmTKKXNfsCOQLutHiluRDsBWTjsuvZ2sc49Wpja6NP9410WNtu53F9TfPG0erFKfEhEi84ceReBBMhIbnKceVmqls5JxigthtpzNx0oxln2Ktt7tdpr762xud48Kzi5vsXH1M4/ib2qn8/2jJ06ajfeTdiJsmKTL4UhpQ7b06c9JHzPntuTcdxFUCDGOXUR4X+hA3XkbejZfVHE/5yf/jBZEzJVrx5NVMjUJIXLCzVV4QsWPXKQH4jL1hmcjKDfvZsW472Fgqtzvue8WTMaC91K5qPfToor96SK/4S0ZyMt1pj/EDncAgGinQQYucJmvvK4zaFRuwCs9Pae+eW2gXvn5+DquQ4gA7ZNkv2aUXbn0u0r1QYOmQvyfAAMA20/UE/yowj0AAAAASUVORK5CYII=",
              "contentType": "image/png"
            }
          ]
    },
    "templateName": "TestRazor",
    "data": "{\"Name\":\"test name\",\"Description\":\"test description\"}"
}
```

If needed to have more samples would be better to create a separate page with samples for testing and check service for working properly. It can helps to test different scenarios.

## Links

1. [ASP.NET Core API](https://docs.microsoft.com/en-us/aspnet/core/web-api/?view=aspnetcore-2.2)
