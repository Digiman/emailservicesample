# Email Service Design

**Status: In Development.**

This document provide a vision on design of the system in whole.

## Introduction

Basically service is as independent application that can be developed in parallel and nor reflect the changes in the system in any case.
Main idea is here is to provide the one single way how to send emails and with which settings like SMTP server and etc. 

![Image 1 - High level vision](\images\NewSampleEmailService-Feb2019-HighLevelVision.png)

Image 1 - High level vision

This is very high level view on the system.

## More details

Service already build and successfully working and can provide basic functionality as sending emails and also extended - send emails with templates. It also support sending files with emails (note that to send large files it can tale some time because data need to be loaded in memory and after this send through HTTP with TCP).

![Image 2 - System libraries](\images\NewSampleEmailService-Feb2019-Libraries-ServiceSample.png)

Image 2 - System libraries

On the image 3 provided some implementation logic with classes used to do all the work inside core of the application. API not shown here because it very simple - Controllers use EmailService instance that works as facade for all core logic and provide only actual methods to use.

![Image 3 - Some classes, implementation dependencies](\images\NewSampleEmailService-Feb2019-Classes-ServiceSample.png)

Image 3 - Some classes, implementation dependencies

## Extension points

Service, of cause, has extension point to be extended in future if needed based on new requirements and functionality that need to be handled or provided.

It's very easy to extend existed logic and add new. But make sure that it really required and needed. In this case it can be developed a new class with logic and injected in place where it really needed.

## Notes

Audit functionality in core not created yet. For now it empty methods that later we can update and add actual logic to track all sent emails from the service. It will helps to identify what emails has been sent and what not delivered or etc.

Logging functionality works if runs application locally - it will write to console. By using special Sinks for Serilog it can be simply extended to send logs to Application Insights, Splunk and etc.

## Links

* [Serilog](https://serilog.net/) - extendable structure logger.
* [Serilog Sinks](https://github.com/serilog/serilog/wiki/Provided-Sinks) - list of all available sinks to send and store logs.
* [Application Insights](https://docs.microsoft.com/en-us/azure/azure-monitor/app/app-insights-overview)
* [Splunk](https://www.splunk.com/)