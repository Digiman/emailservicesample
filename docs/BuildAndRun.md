# Information about building and running the service and applications

**Status: In Development.**

In this document provided some details and information about build the projects, publish, create packages and etc. It's more for general information how to deal with a new tools and application that developed with new version of the .NET platform that are future of the .NET platform and provided more possibilities for new applications.

## Using dotnet cli

This console tool provide a lot of flexibility and functionality to work with .NET core applications - build, run, debug, test, publish, package and etc. It provided commands to create projects, solutions, add projects to solution and all from the console. It can be used to develop application without IDE and using simple editor.
Most useful things here is building, running, publishing and packaging that need to use in DevOps workflow. By using using this commands it can be configured and setup to have whole process flow.

### Build

Build application:
```
dotnet build <ProjectName>
```

More details [here](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-build?tabs=netcore2x).

### Run

Run API locally from command line with **dotnet cli** (from build results):
```
dotnet EmailService.API.dll --environment <EnvirommentName>
```

Sample to run service with Development environment configuration:
```
dotnet EmailService.API.dll --environment Development
```

If you not provide a environment name it will run in default mode with Production environment:
```
dotnet EmailService.API.dll
```
or
```
dotnet EmailService.API.dll --environment Production
```

Tun run service from the console also can be user project without building results. It will automatically build and run project on success:
```
dotnet run <ProjectFolder>
```
It will run application and you can do execute requests.

More details [here](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-run?tabs=netcore21).

### Publish

To create publish package for service it needed to execute command:
```
dotnet publish
```
This command need to execute in he folder with project with API!

.NET core also provided functionality to build self contained applications without installing SDK and runtime to machine to deploy. More details is [here](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-publish?tabs=netcore21).
```
dotnet publish -c Release --self-contained -r win-x64
```
It will create also executable that can simply run to start locally web service and start calling it.

### Packaging

Because .NET core and .NET standard applications is very independent from platform it also provide a new base functionality from the box like creating NuGet packages. It really can helps to build packages very fast and without additional tools and files. But if needed to build packages with more that one library it needed to use *.nuspec file that will provide list of libraries to put in package and additional details. If it needed to pack only one project it enough to put details on package in the project settings and enable creating packages on the project level.

Create a NuGet package:
```
dotnet pack
```

If it runs in project folder (not solution but project, *.csproj file) it will create package for this project.

In case of more complex package it need to create a specification file (*.nuspec) with details about package and all files inside the package. After this it can be executed with next command to create a package:
```
dotnet pack EmailService.Consumer.csproj /p:NuspecFile=EmailService.Consumer.nuspec -c Release
```

Details [here](https://docs.microsoft.com/en-us/dotnet/core/tools/dotnet-pack?tabs=netcore2x).

## Using Cake

Created sample cake build file that included some tasks that can generate artifacts for service and create NuGet package for Consumer library.
Tasks descriptions:

1. **CleanAll** - clean all projects, solutions and artifacts folder. 
2. **BuildService** - build service solution.
3. **PublishService** - create artifacts for API service and put in in artifacts folder (./artifacts/service).
4. **BuildPackage** - build solution with consumer library and samples.
5. **CreatePackage** - create NuGet package for consumer library (./artifacts/packages).
6. **PublishAndPackage** - publish service artifacts and package.

At this time ut can be used but maybe better to use more simpler dotnet cli because under the hood Cake commands also uses cli to do all actions (clean, restore, build, pack).

More details on build .NET Core application with Cake [here](https://cakebuild.net/dsl/dotnetcore/).

## Packaging with Nuget cli

NuGet provided special tool to use from command line to build and push packages in NuGet repository feed. It can be used here also to have possibility to build apps and tools all from console using script files.

Sample command to create NuGet package from console using nuget cli:
```
nuget pack EmailService.Consumer.nuspec -OutputDirectory ../../artifacts/packages -Version 1.0.1
```

Or it can be run in folder with *.nuspec file and it will save package in same location.

More details [here](https://docs.microsoft.com/en-us/nuget/tools/cli-ref-pack).

## Links

1. [dotnet cli](https://docs.microsoft.com/en-us/dotnet/core/tools/?tabs=netcore2x)
2. [Cake](https://cakebuild.net/)
3. [Continuous integration and deployment](https://docs.microsoft.com/en-us/aspnet/core/azure/devops/cicd?view=aspnetcore-2.2)
