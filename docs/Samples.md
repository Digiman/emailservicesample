# Samples to test API for service

TODO: work in progress for samples document...

## Send simple email

Pattern:

1. Object with data - json model.
2. Expected results.
3. Actual results.

### Sample 1

```json
{
    "fromEmail": "digiman89@gmail.com",
    "to": "akukharenko@exadel.com",
    "subject": "Test mail from with API",
    "body": "Hello, EmailService API email sender!"
}
```
Expected result: Working.
Actual result: Working.

Check for not empty filed:
```json
{
    "fromEmail": "digiman89@gmail.com",
    "to": "",
    "subject": "Test mail from with API",
    "body": "Hello, EmailService API email sender!"
}
```
Expected result: Error.
Actual result: Error.

Check one email to valid value (at least one valid email):
```json
{
    "fromEmail": "digiman89@gmail.com",
    "to": "fadfdfdd",
    "subject": "Test mail from with API",
    "body": "Hello, EmailService API email sender!"
}
```
Expected result: Error.
Actual result: Error.

Check to not more than 2 emails to valid value (all emails valid):
```json
{
    "fromEmail": "digiman89@gmail.com",
    "to": "test@,mmail.ru,test2@mail.ru,test3@mail,ru",
    "subject": "Test mail from with API",
    "body": "Hello, EmailService API email sender!"
}
```
Expected result: Error.
Actual result: Error.

## Send email with template

