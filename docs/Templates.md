## Working with templates

## Introduction

Application has functionality to generate email body from template - create HTML content.
It supports 2 types of format:

1. Markdown.
2. Razor (cshtml).

Inside application for this created Template Engines. This engines handles transformation logic - from initial template and data to final HTML result.

## Template engines

Created 3 engines:

1. Markdown engine - to transform Markdown files.
2. Razor Engine - to transform Razor templates.
3. Hybrid engine - to transform Markdown file with Razor content (it used Razor to insert data to template and later Markdown to convert to HTML).

It means from this point of view that would be better to use Hybrid templates because it more simpler to create  - Markdown files is a text files with very simple syntax.

## Internals

Internally template generation logic and all elated stuff suited in one library/project - **EmailService.Core.Template**.
This project build with .NET Standard 2.0 and it means it capable with .NET Framework 4.6.1 and .NET Core 2.0+.
Here exists 2 generators:

* **MarkdownTemplateGenerator** - actual logic to use MarkDig to transform Markdown template to HTML.
* **RazorTemplateGenerator** - actual logic to use RazorLight library to transform template with data into HTML.

And 3 engines:

* **HybridTemplateEngine** - engine that support template in Markdown format but with data injection with Razor syntax.
* **MarkdownTemplateEngine** - engine that support simple Markdown templates without active data (static).
* **RazorTemplateEngine** - engine that support templates in Razor format with data. It usually provided as CSHTML file (C# syntax inside HTML). It means here it possible to add some styles and HTML native things if needed.

## Notes

Additional customization and logic here for now noe needed but in case if it required it can be very simple extended with current engines, generators and more. Also to have better design it build with using interfaces and DI that allow to configure actual implementation.

## Links

1. [Markdown format](https://commonmark.org/) 
2. [MarkDig](https://github.com/lunet-io/markdig)
3. [RazorLight](https://github.com/toddams/RazorLight) 