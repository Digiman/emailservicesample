﻿using System;
using EmailService.Consumer.Configuration;
using EmailService.Consumer.Consumers;
using EmailService.Consumer.Models;

namespace EmailService.Samples.ConsolePackageAppF
{
    static class Program
    {
        static void Main(string[] args)
        {
            try
            {
                DoSomething();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        private static void DoSomething()
        {
            // create settings for email service in place
            //var consumer = new EmailServiceConsumer(new EmailServiceConfiguration
            //{
            //    BaseApiPath = "http://localhost:5000"
            //});

            // load from default configuration file inside consumer library
            var consumer = new EmailServiceConsumer();

            consumer.SendSimpleEmail(new EmailModel
            {
                FromDisplayName = "Andrey Kukharenko",
                FromEmail = "digiman89@gmail.com",
                To = "akukharenko@exadel.com",
                Subject = "Sample email from app with NuGet package",
                Body = "some simple text..."
            });

            Console.WriteLine("Email successfully sent!");
        }
    }
}
