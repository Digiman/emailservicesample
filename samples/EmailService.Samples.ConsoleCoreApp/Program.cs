﻿using System;
using EmailService.Samples.Common.Helpers;

namespace EmailService.Samples.ConsoleCoreApp
{
    /// <summary>
    /// Entry point into application.
    /// </summary>
    static class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Running samples...");

            try
            {
                // 1. Sync methods sample.
                SamplesHelper.MainInternal();

                // 2. Async methods sample.
                SamplesHelper.MainInternalAsync().GetAwaiter().GetResult();

                Console.WriteLine("Samples executed successfully!");
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error: " + ex.Message);
            }

            Console.WriteLine("------------------------------------------------------");
            Console.WriteLine("Press any key to exit...");
            Console.ReadKey();
        }
    }
}
