﻿using System;
using System.Threading.Tasks;
using EmailService.Samples.Common.Samples;

namespace EmailService.Samples.Common.Helpers
{
    /// <summary>
    /// Helper class with some logic to run samples from console applications.
    /// </summary>
    public static class SamplesHelper
    {
        /// <summary>
        /// Main methods to call actual samples.
        /// </summary>
        public static void MainInternal()
        {
            RunSamples();
        }

        /// <summary>
        /// Main methods to call actual samples.
        /// </summary>
        public static async Task MainInternalAsync()
        {
            await RunSamplesAsync();
        }

        /// <summary>
        /// Run sample methods in sync mode.
        /// </summary>
        private static void RunSamples()
        {
            Console.WriteLine("Run email samples in sync...");

            // 1. Send simple email.
            var simpleSamples = new SendSimpleEmailSample();
            simpleSamples.Execute();

            // 2. Send email with template.
            var templateSamples = new SendEmailWithTemplateSample();
            templateSamples.Execute();

            Console.WriteLine("Finish executing sync samples");
        }

        /// <summary>
        /// Run sample methods in async mode.
        /// </summary>
        private static async Task RunSamplesAsync()
        {
            Console.WriteLine("Run email samples in async...");

            // 1. Send simple email.
            var simpleSamples = new SendSimpleEmailSample();
            await simpleSamples.ExecuteAsync();

            // 2. Send email with template.
            var templateSamples = new SendEmailWithTemplateSample();
            await templateSamples.ExecuteAsync();

            Console.WriteLine("Finish executing async samples");
        }
    }
}
