﻿using System.Threading.Tasks;

namespace EmailService.Samples.Common.Interface
{
    /// <summary>
    /// Simple logic to show samples to execute for testing purpose.
    /// </summary>
    public interface ISample
    {
        /// <summary>
        /// Execute samples.
        /// </summary>
        void Execute();

        /// <summary>
        /// Execute samples.
        /// </summary>
        Task ExecuteAsync();
    }
}
