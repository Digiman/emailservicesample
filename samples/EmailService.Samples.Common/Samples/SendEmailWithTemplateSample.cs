﻿using System.Threading.Tasks;
using EmailService.Consumer.Interfaces;
using EmailService.Consumer.Models;
using EmailService.Samples.Common.Interface;
using EmailService.Samples.Common.MailData;

namespace EmailService.Samples.Common.Samples
{
    /// <summary>
    /// Samples how to use Consumer library from EmailService to send emails with templates.
    /// </summary>
    public class SendEmailWithTemplateSample : EmailSampleBase, ISample
    {
        /// <summary>
        /// Actual consumer instance to send email with service.
        /// </summary>
        private readonly IEmailServiceConsumer _consumer;

        public SendEmailWithTemplateSample()
        {
            _consumer = GetDefaultConsumer();
            // or
            //_consumer = GetConsumerWithConfiguration();
        }

        /// <summary>
        /// Execute samples.
        /// </summary>
        public void Execute()
        {
            SendEmail();

            SendEmailWithAttachment();
        }

        /// <summary>
        /// Execute samples.
        /// </summary>
        public async Task ExecuteAsync()
        {
            await SendEmailAsync();

            await SendEmailWithAttachmentAsync();
        }

        #region Actual samples with logic to send emails.

        #region Sync methods.

        /// <summary>
        /// Sample 1. Send email with body that will be generated from template (without attachments).
        /// </summary>
        private void SendEmail()
        {
            // 1. Create model with information about email recipients.
            var model = new EmailTemplateModel
            {
                Model = new EmailModel
                {
                    FromEmail = "digiman89@gmail.com",
                    To = "akukharenko@exadel.com",
                    Subject = "Test email from new EmailService [ConsumerConsoleApp]"
                },
                TemplateName = "TestHybrid",
                Data = new TestEmailMailData
                {
                    Name = "test name - Consumer",
                    Description = "test description - Consumer"
                }
            };

            // 2. Send email.
            _consumer.SendEmailWithTemplate(model);
        }

        /// <summary>
        /// Sample 2. Send email with body that will be generated from template with attachments.
        /// </summary>
        private void SendEmailWithAttachment()
        {
            // 1. Create model with information about email recipients.
            var model = new EmailTemplateModel
            {
                Model = new EmailModel
                {
                    FromEmail = "digiman89@gmail.com",
                    To = "akukharenko@exadel.com",
                    Subject = "Test email from new EmailService [ConsumerConsoleApp]"
                },
                TemplateName = "TestHybrid",
                Data = new TestEmailMailData
                {
                    Name = "test name - Consumer",
                    Description = "test description - Consumer"
                }
            };

            // 1.1. Add files as attachments.
            model.Model.Files = AddFiles();

            // 2. Send email.
            _consumer.SendEmailWithTemplate(model);
        }

        #endregion

        #region Async methods.

        /// <summary>
        /// Sample 1. Send email with body that will be generated from template (without attachments).
        /// </summary>
        private async Task SendEmailAsync()
        {
            // 1. Create model with information about email recipients.
            var model = new EmailTemplateModel
            {
                Model = new EmailModel
                {
                    FromEmail = "digiman89@gmail.com",
                    To = "akukharenko@exadel.com",
                    Subject = "Test email from new EmailService [ConsumerConsoleApp]"
                },
                TemplateName = "TestHybrid",
                Data = new TestEmailMailData
                {
                    Name = "test name - Consumer",
                    Description = "test description - Consumer"
                }
            };

            // 2. Send email.
            await _consumer.SendEmailWithTemplateAsync(model);
        }

        /// <summary>
        /// Sample 2. Send email with body that will be generated from template with attachments.
        /// </summary>
        private async Task SendEmailWithAttachmentAsync()
        {
            // 1. Create model with information about email recipients.
            var model = new EmailTemplateModel
            {
                Model = new EmailModel
                {
                    FromEmail = "digiman89@gmail.com",
                    To = "akukharenko@exadel.com",
                    Subject = "Test email from new EmailService [ConsumerConsoleApp]"
                },
                TemplateName = "TestHybrid",
                Data = new TestEmailMailData
                {
                    Name = "test name - Consumer",
                    Description = "test description - Consumer"
                }
            };

            // 1.1. Add files as attachments.
            model.Model.Files = AddFiles();

            // 2. Send email.
            await _consumer.SendEmailWithTemplateAsync(model);
        }

        #endregion

        #endregion
    }
}
