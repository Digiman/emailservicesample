﻿using System.Collections.Generic;
using System.IO;
using EmailService.Consumer.Configuration;
using EmailService.Consumer.Consumers;
using EmailService.Consumer.Interfaces;
using EmailService.Consumer.Models;
using HeyRed.Mime;

namespace EmailService.Samples.Common.Samples
{
    /// <summary>
    /// Base class for samples.
    /// </summary>
    public abstract class EmailSampleBase
    {
        /// <summary>
        /// Read some static sample files from local machine to use in attachments for emails.
        /// </summary>
        /// <returns>Returns a list of files to attach to email.</returns>
        protected List<FileModel> AddFiles()
        {
            // TODO: hardcoded list of files for sample only!
            string[] files = new[]
            {
                "App_Data\\E-470 Logo.png",
                "App_Data\\E-470 Logo 2.png",
                "App_Data\\E-470 Fact Sheet Infographic.pdf"
            };

            var result = new List<FileModel>();

            var currentDirectory = Directory.GetCurrentDirectory();

            foreach (var file in files)
            {
                result.Add(LoadFile(Path.Combine(currentDirectory, file)));
            }

            return result;
        }

        /// <summary>
        /// Load file from the disk in FileModel object.
        /// </summary>
        /// <param name="filename">File to load.</param>
        /// <returns>Returns object with loaded file and required details.</returns>
        private FileModel LoadFile(string filename)
        {
            return new FileModel
            {
                Content = File.ReadAllBytes(filename),
                FileName = Path.GetFileName(filename),
                ContentType = GetMimeType(filename)
            };
        }

        /// <summary>
        /// Identify MIME type for file. It required to send later as attachment with proper type.
        /// </summary>
        /// <param name="fileName">File to identity the type.</param>
        /// <returns>Returns MIME type.</returns>
        private string GetMimeType(string fileName)
        {
            return MimeGuesser.GuessMimeType(fileName);
        }

        /// <summary>
        /// Get consumer with default configuration (loaded from default file).
        /// </summary>
        /// <returns>Returns instance on the class to send emails.</returns>
        protected IEmailServiceConsumer GetDefaultConsumer()
        {
            return new EmailServiceConsumer();
        }

        /// <summary>
        /// Get consumer with predefined configuration here - in current application that want to send emails.
        /// </summary>
        /// <returns>Returns instance on the class to send emails.</returns>
        protected IEmailServiceConsumer GetConsumerWithConfiguration()
        {
            // TODO: here configuration for EmailService created in consumer application and send to library constructors
            var configuration = new EmailServiceConfiguration
            {
                BaseApiPath = "http://localhost:5000", // this values can be read from configuration for application (not hard-code this value in actual applications!)
                ApiVersion = "1.0"
            };

            return new EmailServiceConsumer(configuration);
        }
    }
}
