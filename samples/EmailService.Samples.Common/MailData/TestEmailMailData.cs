﻿using System;

namespace EmailService.Samples.Common.MailData
{
    /// <summary>
    /// Test model to use in test template.
    /// </summary>
    public sealed class TestEmailMailData
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public int Year => DateTime.Today.Year;
    }
}
