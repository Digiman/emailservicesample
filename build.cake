//////////////////////////////////////////////////////////////////////
// ARGUMENTS
//////////////////////////////////////////////////////////////////////

// base arguments
var target = Argument("target", "Default");
var configuration = Argument("configuration", "Release");

// environment name
var environment = Argument("environment", "Production");

// solutions
var solutionService = Argument("solutionService", "EmailService.sln");
var solutionPackage = Argument("solutionPackage", "EmailService.Consumer.sln");

// project names
// var apiProjectName = Argument("apiProjectName", "EmailService.Api");

// project files
var apiProjectPath = Argument("apiProjectPath", "./src/EmailService.API/EmailService.API.csproj");
var consumerProjectPath = Argument("consumerLibraryPath", "./src/EmailService.Consumer/EmailService.Consumer.csproj");

// projects path
var apiProject = Argument("consumerLibrary", "./src/EmailService.API");
var consumerLibrary = Argument("consumerLibrary", "./src/EmailService.Consumer");

// nuspec file for package
var consumerLibrarySpec = Argument("consumerLibrary", "./src/EmailService.Consumer/EmailService.Consumer.nuspec");

//////////////////////////////////////////////////////////////////////
// PREPARATION
//////////////////////////////////////////////////////////////////////

var servicePublishPath = Directory("./artifacts/service/");
var packagePublishPath = Directory("./artifacts/packages/");

// path to API main library to start the service locally
// var apiLibraryFullPath = servicePublishPath + File("EmailService.Api.dll");

//////////////////////////////////////////////////////////////////////
// TASKS
//////////////////////////////////////////////////////////////////////

Task("Info").Does(() => {
    Information("Configuration: " + configuration);
});

Task("CleanService")
.IsDependentOn("Info")
.Does(() => {    
    CleanDirectory(servicePublishPath);

    var settings = new DotNetCoreCleanSettings
    {
        //Framework = "netcoreapp2.2",
        Configuration = configuration,
    };

    DotNetCoreClean(solutionService, settings);
});

Task("RestoreService")
.IsDependentOn("CleanService")
.Does(() => {
    var settings = new DotNetCoreRestoreSettings
    {
        // Sources = new[] {"https://www.example.com/nugetfeed", "https://www.example.com/nugetfeed2"},
        // FallbackSources = new[] {"https://www.example.com/fallbacknugetfeed"},
        // PackagesDirectory = "./packages",
        // Verbosity = Information,
        // DisableParallel = true,
        // InferRuntimes = new[] {"runtime1", "runtime2"}
    };

    DotNetCoreRestore(solutionService, settings);
});

Task("BuildService")
.IsDependentOn("RestoreService")
.Does(() => {
    var settings = new DotNetCoreBuildSettings
    {
        Configuration = configuration
    };

    DotNetCoreBuild(solutionService, settings);
});

Task("CopyScriptsService")
.Does(() => {
    var files = new [] {
        "./scripts/RunServiceDev.cmd",
        "./scripts/RunServiceProd.cmd"
    };
    CopyFiles(files, servicePublishPath);
});

Task("PublishService")
.IsDependentOn("BuildService")
.IsDependentOn("CopyScriptsService")
//.IsDependentOn("CleanService") // or use only cleaning
.Does(() => {
    var settings = new DotNetCorePublishSettings
    {
        //Framework = "netcoreapp2.2",
        Configuration = configuration,
        OutputDirectory = servicePublishPath
    };

    DotNetCorePublish(apiProjectPath, settings);
});

Task("PublishServiceSelf")
.IsDependentOn("BuildService")
//.IsDependentOn("CleanService") // or use only cleaning
.Does(() => {
    var settings = new DotNetCorePublishSettings
    {
        //Framework = "netcoreapp2.2",
        Configuration = configuration,
        OutputDirectory = servicePublishPath,
        SelfContained = true,
        Runtime = "win-x64" // here it can be provided all Runtime Identifiers to target
    };

    DotNetCorePublish(apiProjectPath, settings);
});

//----------------------------------------------------------------------
// Tasks for build and prepare NuGet package for Consumer library
//----------------------------------------------------------------------

Task("CleanPackage")
.IsDependentOn("Info")
.Does(() => {
    CleanDirectory(packagePublishPath);

    var settings = new DotNetCoreCleanSettings
    {
        Configuration = configuration,
    };

    DotNetCoreClean(solutionPackage, settings);
});

Task("RestorePackage")
.IsDependentOn("CleanPackage")
.Does(() => {
    var settings = new DotNetCoreRestoreSettings
    {
    };

    DotNetCoreRestore(solutionPackage, settings);
});

Task("BuildPackage")
.IsDependentOn("RestorePackage")
.Does(() => {
    var settings = new DotNetCoreBuildSettings
    {
        Configuration = configuration
    };

    DotNetCoreBuild(solutionPackage, settings);
});

Task("CreatePackage")
.IsDependentOn("BuildPackage")
.Does(() => {
    var settings = new DotNetCorePackSettings
    {
        Configuration = configuration,
        OutputDirectory = packagePublishPath
    };

    DotNetCorePack(consumerLibrary, settings);
});

Task("CreatePackageFromSpec")
.IsDependentOn("BuildPackage")
.Does(() => {
    var settings = new DotNetCorePackSettings
    {
        Configuration = configuration,
        OutputDirectory = packagePublishPath
    };

    DotNetCorePack(consumerLibrarySpec, settings);
});

//---------------------------------------------------------
// Other related to the project tasks.
//---------------------------------------------------------

Task("CleanAll")
.IsDependentOn("CleanService")
.IsDependentOn("CleanPackage")
.Does(() => {
    Information("Clean all...");
});

//----------------------------------------------------------------------
// Special aggregated tasks.
//----------------------------------------------------------------------

Task("PublishAndPackage")
.IsDependentOn("PublishService")
.IsDependentOn("CreatePackage")
.Does(() => {
    Information("Publish service artifacts and Generarte Consumer Package...");
});

//----------------------------------------------------------------------
// Task to run API service locally from console command.
// CLI command: > dotnet run EmailService.API.csproj
//----------------------------------------------------------------------

Task("RunService")
.Does(() => {
    DotNetCoreRun(apiProjectPath, "--environment " + environment);
});

//////////////////////////////////////////////////////////////////////
// EXECUTION
//////////////////////////////////////////////////////////////////////

RunTarget(target);
